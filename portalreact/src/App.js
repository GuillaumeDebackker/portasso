import {
  BrowserRouter as Router,
  Switch,
  Route
} from 'react-router-dom';
import Index from './pages/Index';
import Admin from './pages/Admin';

import 'bootstrap/dist/css/bootstrap.min.css';
import 'jquery';
 
function App() {
  return (
    <div className="App">
        <Router>
          <Switch>
              <Route path="/admin" component={Admin}/>
              <Route path='/' component={Index} />
          </Switch>
        </Router>
    </div>
  );
}

export default App;
