import React from 'react';
import {Link,Redirect} from 'react-router-dom';
import Axios from 'axios';
import ConfigWebsite from "../../config/ConfigWebsite";

export default class LoginForm extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            redirect: false,
            login: false,
            email: "",
            password: "",
            remember: ""
        };
        this.dangerArray = [];
        this.checkSubmit();
    }

    submit = async(event) => {
        const body = {
            "emailLogin" : this.state.email,
            "passwordLogin" : this.state.password,
        }
        const {data} = await Axios.post(`${ConfigWebsite.HOST}:${ConfigWebsite.PORTAPI}/user/login`,body);
        if(data.message !== undefined){
            this.props.setState({message:data.message,message_type:data.type});

            this.setState({email: "",password: ""});
        }else{
            this.setState({redirect: true});
            localStorage.setItem('user',data.userId);
            window.location.reload(false);
        }
    }

    checkSubmit = () => {
        let disabled = this.dangerArray.length !== 0;
        if(!disabled){
            const inputs = document.querySelectorAll(".login");

            for (let index = 0; index < inputs.length; index++) {
                const element = inputs[index];
                if(element.value === ""){
                    disabled = true;
                } 
            }
        }
        this.setState({login : disabled})
        setTimeout(this.checkSubmit,100);
    }

    updateEmail = (e) => {
        if(e.target.value === ""){
            this.setDanger(e.target.id);
        }else{
            this.setDanger(e.target.id, !this.hasMatches(e.target.value,"@"), (!this.hasMatches(e.target.value,"@") ? "Vous devez mettre un mail" : ""));
        }
        this.setState({email:e.target.value});
    }

    getPassword = (e) => {
        const password = e.target;
        if(this.hasMatches(password.value,/\s/)){
            this.setDanger(password.id, true, "Les espaces sont interdit");
        }else if(password.value.length < 8 && password.value !== ""){
            this.setDanger(password.id, true, "Le mot de passe doit contenir 8 character");
        }else{
            this.setDanger(password.id);
        }

        this.setState({password: password.value});
    }

    setDanger(id,danger = false,message = ""){
        if(danger){
            if(!this.hasDanger(id)) this.dangerArray.push(id);
            document.getElementById("input" +id).className = "input-group-prepend danger";
            document.getElementById("error" +id).innerHTML = message;
        }else{
            this.dangerArray.splice(this.dangerArray.indexOf(id),1);
            document.getElementById("input" +id).className = "input-group-prepend";
            document.getElementById("error" +id).innerHTML = "";
        }
    }

    hasDanger(id) {
        for (let index = 0; index < this.dangerArray.length; index++) {
            const element = this.dangerArray[index];
            if(element === id) return true;
        }
        return false;
    }

    hasMatches(value,reg){
        reg = new RegExp(reg,"g");
        const matches = value.match(reg);
        return matches && matches.length > 0;
    }

    render(){
        if(this.state.redirect) return <Redirect to="/"></Redirect>
        return(
            <div className="card card-sign">
                <div className="card-header">
                    <h3>Connexion</h3>
                </div>
                <div className="card-body">
                    <div className="input-group form-group">
                        <div className="input-group-prepend" id="inputemailLogin">
                            <span className="input-group-text register"><i className="fa fa-envelope"></i></span>
                        </div>
                        <input name="emailLogin" id="emailLogin" type="email" className="form-control login" placeholder="Email" value={this.state.email} onChange={this.updateEmail} />
                        <div id="erroremailLogin" className="error"></div>
                    </div>
                    <div className="input-group form-group">
                        <div className="input-group-prepend" id="inputpasswordLogin">
                            <span className="input-group-text register"><i className="fa fa-key"></i></span>
                        </div>
                        <input name="passwordLogin" id="passwordLogin" type="password" className="form-control login" placeholder="Mot de passe" value={this.state.password} onChange={this.getPassword} />
                        <div id="errorpasswordLogin" className="error"></div>	
                    </div>
                    
                    <div className="row align-items-center remember">
                        <input type="checkbox" name="remember" /> Souvenir de moi 
                    </div>
                    <div className="form-group">
                        <input type="submit" id="connexion" value="Valider" className="btn float-right login_btn" onClick={this.submit} disabled={this.state.login} />
                    </div>
                </div>
                <div className="card-footer">
                    <div className="d-flex justify-content-center">
                        <Link to="/">Mot de passe oublié?</Link>
                    </div>
                </div>
            </div>
        );
    }
}