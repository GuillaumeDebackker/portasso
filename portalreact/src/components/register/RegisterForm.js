import React from 'react';
import Axios from 'axios';
import ConfigWebsite from "../../config/ConfigWebsite";

export default class RegisterForm extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            CGU: false,
            register: false,
            firstname: "",
            lastname: "",
            email: "",
            password: "",
        }
        this.dangerArray = [];
        this.checkSubmit();
    }

    submit = async(e) => {
        const body = {
            "firstname" : this.state.firstname,
            "lastname" : this.state.lastname,
            "email" : this.state.email,
            "password" : this.state.password,
        }
        const {data} = await Axios.post(`${ConfigWebsite.HOST}:${ConfigWebsite.PORTAPI}/user/register`,body);
        this.props.setState({message:data.message,message_type:data.type});

        this.setState({
            firstname: "",
            lastname: "",
            email: "",
            password: ""
        });
        const password = document.getElementById("passwordConfirm");
        password.value="";
    }

    nameInput = (e) => {
        if(this.hasMatches(e.target.value,"[0-9]")){
            this.setDanger(e.target.id,true,"Vous devez utiliser que des lettres");
        }else{
            this.setDanger(e.target.id);
        }
        (e.target.id === "firstname" ? this.setState({firstname:e.target.value}) : this.setState({lastname:e.target.value}));
    }

    email = (e) => {
        if(e.target.value === ""){
            this.setDanger(e.target.id);
        }else{
            this.setDanger(e.target.id, !this.hasMatches(e.target.value,"@"), (!this.hasMatches(e.target.value,"@") ? "Vous devez mettre un mail" : ""));
        }
        this.setState({email:e.target.value});
    }

    checkSubmit = () => {
        let disabled = this.dangerArray.length !== 0 || !this.state.CGU;
        if(!disabled){
            const inputs = document.querySelectorAll(".register");

            for (let index = 0; index < inputs.length; index++) {
                const element = inputs[index];
                if(element.value === ""){
                    disabled = true;
                } 
            }
        }
        this.setState({register : disabled})
        setTimeout(this.checkSubmit,100);
    }

    CGU = (e) => {
        this.setState({CGU : e.target.value});
    }
 
    getPassword = (e) => {
        const password = document.getElementById("password");
        const passwordConfirm = document.getElementById("passwordConfirm");
        if(this.hasMatches(password.value,/\s/)){
            this.setDanger(password.id, true, "Les espaces sont interdit");
        }else if(password.value.length < 8 && password.value !== ""){
            this.setDanger(password.id, true, "Le mot de passe doit contenir 8 character");
        }else{
            this.setDanger(password.id);
        }
        
        if(passwordConfirm.value !== "" && passwordConfirm.value !== password.value){
            this.setDanger(passwordConfirm.id, true, "Le mot de passe doit être identique");
        }else{
            this.setDanger(passwordConfirm.id);
        }

        this.setState({password: password.value})
    }

    setDanger(id,danger = false,message = ""){
        if(danger){
            if(!this.hasDanger(id)) this.dangerArray.push(id);
            document.getElementById("input" +id).className = "input-group-prepend danger";
            document.getElementById("error" +id).innerHTML = message;
        }else{
            this.dangerArray.splice(this.dangerArray.indexOf(id),1);
            document.getElementById("input" +id).className = "input-group-prepend";
            document.getElementById("error" +id).innerHTML = "";
        }
    }

    hasDanger(id) {
        for (let index = 0; index < this.dangerArray.length; index++) {
            const element = this.dangerArray[index];
            if(element === id) return true;
        }
        return false;
    }

    hasMatches(value,reg){
        reg = new RegExp(reg,"g");
        const matches = value.match(reg);
        return matches && matches.length > 0;
    }

    render(){
        return(
            <div className="card card-sign">
                <div className="card-header">
                    <h3>S'enregistrer</h3>
                </div>
                
                <div className="card-body">
                    <div className="input-group form-group">
                        <div className="input-group-prepend" id="inputfirstname">
                            <span className="input-group-text register"><i className="fa fa-user"></i></span>
                        </div>
                        <input name="firstname" id="firstname" type="text" className="form-control register" value={this.state.firstname} onChange={this.nameInput} placeholder="Prénom" />
                        <div id="errorfirstname" className="error"></div>
                    </div>
                    <div className="input-group form-group">
                        <div className="input-group-prepend" id="inputlastname">
                            <span className="input-group-text register"><i className="fa fa-user-plus"></i></span>
                        </div>
                        <input name="lastname" id="lastname" type="text" className="form-control register" value={this.state.lastname} onChange={this.nameInput} placeholder="Nom de famille" />
                        <div id="errorlastname" className="error"></div>
                    </div>
                    <div className="input-group form-group">
                        <div className="input-group-prepend" id="inputemail">
                            <span className="input-group-text register"><i className="fa fa-envelope"></i></span>
                        </div>
                        <input name="email" id="email" type="email" className="form-control register" value={this.state.email} onChange={this.email} placeholder="Email" />
                        <div id="erroremail" className="error"></div>
                    </div>
                    <div className="input-group form-group">
                        <div className="input-group-prepend" id="inputpassword">
                            <span className="input-group-text register"><i className="fa fa-key"></i></span>
                        </div>
                        <input name="password" id="password" type="password" className="form-control register" value={this.state.password} onChange={this.getPassword} placeholder="Mot de passe" />
                        <div id="errorpassword" className="error"></div>
                    </div>
                    <div className="input-group form-group">
                        <div className="input-group-prepend" id="inputpasswordConfirm">
                            <span className="input-group-text register"><i className="fa fa-key"></i></span>
                        </div>
                        <input name="passwordConfirm" id="passwordConfirm" type="password" className="form-control register" onChange={this.getPassword} placeholder="Confirmer votre mot de passe"/>
                        <div id="errorpasswordConfirm" className="error"></div>
                    </div>
                    
                    <div className="row align-items-center remember">
                        <input type="checkbox" name="CGU" id="CGU" checked={this.state.CGU} onChange={this.CGU} /> Accepter les conditions d'utilisation
                    </div>
                    <div className="form-group">
                        <button id="validateregister" className="btn float-right login_btn" onClick={this.submit} disabled={this.state.register}>Valider</button>
                    </div>
                </div>
            </div>
        );
    }
}