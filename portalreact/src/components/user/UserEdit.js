import React from 'react';
import Axios from 'axios'
import ConfigWebsite from "../../config/ConfigWebsite";

export default class UserEdit extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            firstname: this.props.user.firstname,
            lastname: this.props.user.lastname,
            email: this.props.user.email,
            rank: this.props.user.role
        }
    }

    submit = async() => {
        const body = {
            "_id": this.props.user._id,
            "firstname" : this.state.firstname,
            "lastname" : this.state.lastname,
            "email" : this.state.email,
            "rank" : this.state.rank
        }


        const {data} = await Axios.post(`${ConfigWebsite.HOST}:${ConfigWebsite.PORTAPI}/user/edit`,body);
        this.props.setState({message:data.message,message_type:data.type,mode: 0});
    }

    updateFirstname = (event) => this.setState({firstname: event.target.value});
    updateLastname = (event) => this.setState({lastname: event.target.value});
    updateEmail = (event) => this.setState({email: event.target.value});
    updateRank = (event) => this.setState({rank: event.target.value});

    render(){
        return(
            <div className="card">
                <div className="card-header bg-card">
                    <h3 className="card-title">Editer un utilisateur</h3>
                    <div className="card-tools">
                        <button type="button" onClick={() => this.props.setState({mode:0})} className="btn btn-tool">
                            <i className="fa fa-times"></i>
                        </button>
                    </div>
                </div>
                <div className="card-body">
                    <div className="row">
                        <div className="col-3">
                            <div className="form-group">
                                <label htmlFor="firstname">Prénom</label>
                                <input type="text" className="form-control" key="firstname" value={this.state.firstname} onChange={this.updateFirstname} required/>
                            </div>
                        </div>
                        <div className="col-3">
                            <div className="form-group">
                                <label htmlFor="lastname">Nom de famille</label>
                                <input type="text" className="form-control" key="lastname" value={this.state.lastname} onChange={this.updateLastname} required/>
                            </div>
                        </div>
                        <div className="col-3">
                            <div className="form-group">
                                <label htmlFor="email">Email</label>
                                <input type="text" className="form-control" key="email" value={this.state.email} onChange={this.updateEmail} required/>
                            </div>
                        </div>
                        <div className="col-3">
                            <div className="form-group">
                                <label htmlFor="rank">Grade</label>
                                <select className="form-control" key="rank" value={this.state.rank} onChange={this.updateRank}>
                                    {
                                        this.props.rank.map((rank) => <option key={rank.id} value={rank.id}>{rank.name}</option>)
                                    }
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="card-footer">
                    <div style={{paddingLeft:95 +"%"}}>
                        <button type="button" onClick={this.submit} className="btn btn-primary">Editer</button>
                    </div>
                </div>
            </div>
        );
    }
}