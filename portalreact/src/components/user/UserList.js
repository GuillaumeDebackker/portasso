import React from 'react';
import UserView from './UserView';

export default class UserList extends React.Component{

    render(){
        return(
            <div className="card">
                <div className="card-header">
                    <h3 className="card-title black">Liste des utilisateurs</h3>
                </div>
                <div className="card-body p-0">
                    <table className="table table-bordered projects">
                        <thead>
                            <tr>
                                <th style={{width: 1+"%"}}>#</th>
                                <th style={{width: 20+"%"}}>Nom de famille</th>
                                <th style={{width: 30+"%"}}>Prénom</th>
                                <th style={{width: 20+"%"}}>email</th>
                                <th style={{width: 20+"%"}}>Grade</th>
                                <th style={{width: 20+"%"}}></th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                this.props.user.map((user) => <UserView key={user._id} user={user} rank={this.props.rank.find(rank => rank.id === user.role)} update={state => this.props.setState(state)} />)
                            }
                        </tbody>
                    </table>
                </div>
            </div>
        );
    }
}