import React from 'react';

export default class UserView extends React.Component{

    render(){
        return(
            <tr>
                <td>{this.props.user._id}</td>
                <td>{this.props.user.lastname}</td>
                <td>{this.props.user.firstname}</td>
                <td>{this.props.user.email}</td>
                <td>{this.props.rank.name}</td>
                <td className="text-right">
                    <button type="button" className="btn btn-info btn-sm" onClick={() => this.props.update({mode: 1,useredit:this.props.user})}>
                        <i className="fa fa-pencil"></i> Editer
                    </button>
                </td>
            </tr>
        )
    }
}