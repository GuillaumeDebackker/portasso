import React from "react";
import FormGroup from "../../box/FormGroup";
import Axios from "axios";
import ConfigWebsite from "../../../config/ConfigWebsite";

export default class Compte extends React.Component{

    constructor(props) {
        super(props);
        this.state = {
            user: null
        }
    }

    async componentDidMount() {
        const {data} = await Axios.post(`${ConfigWebsite.HOST}:${ConfigWebsite.PORTAPI}/user`,{id: localStorage.getItem('user')});
        this.setState({user: data.user});
    }

    handleUser = (key,value) => {
        let user = this.state.user;
        user[key] = value;
        this.setState({user:user});
    }

    handleFirstName = (event) => this.handleUser("firstname",event.target.value);
    handleLastName = (event) => this.handleUser("lastname",event.target.value);
    handleEmail = (event) => this.handleUser("email",event.target.value);

    submit = async() => {
        const body = {
            "_id": this.state.user._id,
            "firstname" : this.state.user.firstname,
            "lastname" : this.state.user.lastname,
            "email" : this.state.user.email,
            "rank" : this.state.user.role
        }
        await Axios.post(`${ConfigWebsite.HOST}:${ConfigWebsite.PORTAPI}/user/edit`,body);
    }

    render() {
        if(this.state.user === null) return <div></div>
        return (
            <div className="card">
                <div className="card-header bg-primary" style={{height: 50 + "px"}}>
                    <h3 className="card-title">Compte</h3>
                </div>
                <div className="card-body" style={{backgroundColor: "#f1f1f1"}}>
                    <div className="row">
                        <div className="col-md-6">
                            <FormGroup type={"text"} keyGroup={"firstname"} label={"Prénom"} value={this.state.user.firstname} onChange={this.handleFirstName}/>
                        </div>
                        <div className="col-md-6">
                            <FormGroup type={"text"} keyGroup={"lastname"} label={"Nom de famille"} value={this.state.user.lastname} onChange={this.handleLastName}/>
                        </div>
                        <div className="col-md-12">
                            <FormGroup type={"text"} keyGroup={"email"} label={"Email"} value={this.state.user.email} onChange={this.handleEmail}/>
                        </div>
                    </div>
                </div>
                <div className="card-footer" style={{backgroundColor: "#f1f1f1"}}>
                    <div style={{paddingLeft: 95 +"%"}}>
                        <button type="button" onClick={this.submit} className="btn btn-primary">Editer</button>
                    </div>
                </div>
            </div>
        );
    }
}