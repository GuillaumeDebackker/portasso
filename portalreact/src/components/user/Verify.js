import React from "react";
import Axios from "axios";
import ConfigWebsite from "../../config/ConfigWebsite";
import {Redirect} from 'react-router-dom';

export default class Verify extends React.Component{

    constructor(props) {
        super(props);
        this.state = {
            message_type: "",
            message: ""
        };
    }

    async componentDidMount() {
        const {data} = await Axios.post(`${ConfigWebsite.HOST}:${ConfigWebsite.PORTAPI}/user/validate`,{id: this.props.match.params.id});
        console.log(data);
        this.setState({
            message_type: data.type,
            message: data.message
        })
    }

    render() {
        if(this.state.message === "") return <div></div>
        return <Redirect to={{
            pathname: "/register",
            state: {
                message_type: this.state.message_type,
                message: this.state.message
            }
        }}/>
    }
}