import React from 'react';
import Profil from '../../public/img/profil.png';
import {Dropdown,DropdownButton} from 'react-bootstrap';
import Axios from 'axios';
import ConfigWebsite from "../../config/ConfigWebsite";

export default class ButtonUser extends React.Component{

    constructor(props){
        super(props);
        this.state = {rank: null,associations: []};
    }

    async componentDidMount(){
        const {data} = await Axios.post(`${ConfigWebsite.HOST}:${ConfigWebsite.PORTAPI}/user`,{id: localStorage.getItem("user")});
        const associations = await Axios.post(`${ConfigWebsite.HOST}:${ConfigWebsite.PORTAPI}/association/findall`,{"members.value" : localStorage.getItem("user")});
        this.setState({rank:data.rank,associations: associations.data});
    }

    deconnection = (event) => {
        localStorage.removeItem('user');
        window.location.reload(false);
    }

    checkAdmin = () => {
        for (const permission in this.state.rank.permission) {
            if(this.state.rank.permission[permission] === "ACCESS_ADMIN") return <Dropdown.Item href={"/admin"}>Admin</Dropdown.Item>;
        }
        return null;
    }

    checkActuality = () => {
        // CHECK PERMISSION
        for (const permission in this.state.rank.permission) {
            if(this.state.rank.permission[permission] === "ACTUALITY_CREATE") return <Dropdown.Item href={"/actuality/create"}>Actualité</Dropdown.Item>;
        }
        // CHECK ASSOCIATION ACTUALITY
        for(const association in this.state.associations){
            if(this.state.associations[association].actuality) return <Dropdown.Item href={"/actuality/create"}>Actualité</Dropdown.Item>;
        }
        return null;
    }
    
    render(){
        return (
            <DropdownButton className="navbar-button"
                menuAlign="right"
                title={<img src={Profil} alt="Profil" />}
                id="dropdown-menu-align-right"
                >
                <Dropdown.Item href={"/user/profil"}>Profil</Dropdown.Item>
                {this.state.rank === null ? null : <this.checkAdmin />}
                {this.state.rank === null ? null : <this.checkActuality />}
                <Dropdown.Divider />
                {
                    this.state.associations.map((association) => {
                        return <Dropdown.Item key={association._id} href={"/association/" + association._id}>{association.name}</Dropdown.Item>
                    })
                }
                {
                    this.state.associations.length > 0 ? <Dropdown.Divider/> : null
                }
                <Dropdown.Item as="button" onClick={this.deconnection}>Deconnection</Dropdown.Item>
            </DropdownButton>
        );
    }
}