import React from 'react';
import {Link} from 'react-router-dom';
import ButtonUser from './ButtonUser';
import ButtonRegister from './ButtonRegister';
import Axios from "axios";
import {NavDropdown} from "react-bootstrap";
import DropdownItem from "react-bootstrap/DropdownItem";
import ConfigWebsite from "../../config/ConfigWebsite";


export default class Navbar extends React.Component{

    constructor(props){
        super(props);

        this.state = {
            user: localStorage.getItem('user'),
            showMobile: false,
            associations: [],
        };
    }

    async componentDidMount(){
        const {data} = await Axios.post(`${ConfigWebsite.HOST}:${ConfigWebsite.PORTAPI}/association/findall`);
        this.setState({associations: data})

        this.updateNavbar();
    }

    updateNavbar = () => {
        const Navbar = document.getElementById("navbar");
        const width = window.innerWidth;
        const location = window.location;
        const scroll = window.pageYOffset;
        if(Navbar !== null){
            if(width < 997){
                Navbar.className = "navbar fixed-top navbar-expand-lg bg-custom";
            }else if(location.pathname !== "/"){
                Navbar.className = "navbar fixed-top navbar-expand-lg bg-custom";
            }else if(scroll >= 40){
                Navbar.className = "navbar fixed-top navbar-expand-lg bg-custom";
            }else{
                Navbar.className = "navbar fixed-top navbar-expand-lg";
            }
        }
        setTimeout(this.updateNavbar,100);
    }

    mobileNavbar = (event) => {
        const Navbar = document.getElementById("navbarNav");
        if(this.state.showMobile){
            Navbar.className = "collapse navbar-collapse";
        }else{
            Navbar.className = "collapse navbar-collapse show";
        }
        this.setState({showMobile: !this.state.showMobile})
    }

    render(){
        return (
            <nav className="navbar fixed-top navbar-expand-lg" id="navbar">
                <Link className="navbar-brand" to="/">PORTASSO</Link>
                <button className="navbar-toggler" type="button" onClick={this.mobileNavbar} data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <i className="fa fa-bars"></i>
                </button>
                <div className="collapse navbar-collapse" id="navbarNav">
                    <ul className="navbar-nav ml-auto">
                        <li className="nav-item">
                            <Link to="/" className="nav-link">Accueil</Link>
                        </li>
                        <NavDropdown id={"association"} title={"Association"}>
                            <DropdownItem href={"/association"}>Toutes</DropdownItem>
                            {
                                this.state.associations.map((association) => {
                                    return <DropdownItem key={association._id} href={"/association/" + association._id}>{association.name}</DropdownItem>
                                })
                            }
                        </NavDropdown>
                        <li className="nav-item">
                            <Link to="/actuality" className="nav-link">Actualité</Link>
                        </li>
                        <li className="nav-item">
                            <Link to="/contact" className="nav-link">Contact</Link>
                        </li>
                    </ul>

                    {
                        (this.state.user === null || this.state.user === undefined ? <ButtonRegister /> : <ButtonUser />)
                    }
                </div>
            </nav>
        )
    }
}