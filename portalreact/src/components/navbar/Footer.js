import React from 'react';
import {Link} from 'react-router-dom';

export default class Footer extends React.Component{
    render(){
        return(
            <footer className="site-footer">
                <div className="container">
                    <div className="row">
                        <div className="col-sm-12 col-md-6">
                            <h6>A propos</h6>
                            <p className="text-justify">Description</p>
                        </div>
                        <div className="col-xs-6 col-md-3"></div>
                        <div className="col-xs-6 col-md-3">
                            <h6>Liens rapides</h6>
                            <ul className="footer-links">
                                <li><Link to="/">A propos</Link></li>
                                <li><Link to="/contact">Nous contacter</Link></li>
                            </ul>
                        </div>
                    </div>
                    <br />
                </div>
                <div className="container">
                    <div className="row">
                        <div className="col-md-8 col-sm-6 col-xs-12">
                            <p className="copyright-text">Copyright &copy; 2020 All Rights Reserved by
                            <Link to="/"> PORTASSO</Link>.
                            </p>
                        </div>

                        <div className="col-md-4 col-sm-6 col-xs-12">
                            <ul className="social-icons">
                                
                            </ul>
                        </div>
                    </div>
                </div>
            </footer>
        );
    }
}