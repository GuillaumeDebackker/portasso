import React from 'react';
import {Link} from 'react-router-dom';

export default class FooterAdmin extends React.Component{
    render(){
        return(
            <footer className="main-footer">
                <strong>Copyright &copy; 2020 <Link className="footer-link" to="">PortAsso.fr</Link>.</strong> All rights
                reserved.
            </footer>
        );
    }
}