import React from 'react';
import { Link } from 'react-router-dom';
import NavItem from './NavItem';
import ConfigNav from '../../config/navitem';

export default class NavbarAdmin extends React.Component{

    render(){
        return (
            <div className="sidebar" data-color="purple" data-background-color="black">
                <div className="logo">
                    <Link to="/" className="simple-text logo-normal">
                        Administration
                    </Link>
                </div>
                <div className="sidebar-wrapper">
                    <ul className="nav">
                        {
                            ConfigNav.map((nav_item) => {
                                const isactive = window.location.pathname === nav_item['to'];
                                if(isactive) return <NavItem to={nav_item['to']} icon={nav_item['icon']} name={nav_item['name']} key={nav_item['name']} active/>
                                return <NavItem to={nav_item['to']} icon={nav_item['icon']} name={nav_item['name']} key={nav_item['name']}/>
                            })
                        }
                    </ul>
                </div>
            </div>
        );
    }
}