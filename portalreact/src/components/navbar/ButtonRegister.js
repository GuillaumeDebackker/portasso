import React from 'react';
import {Link} from 'react-router-dom';

export default class ButtonRegister extends React.Component{
    render(){
        return (
            <div className="nav-item button-connexion">
                <Link to="/register" className="nav-link">Connexion</Link>
            </div>
        );
    }
}