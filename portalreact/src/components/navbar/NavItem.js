import React from 'react';
import { Link } from 'react-router-dom';

export default class NavbarItem extends React.Component{

    render(){
        return(
            <li className={"nav-item" + (this.props.active ? " active" : "")}>
                <Link to={this.props.to} className="nav-link">
                    <i className="material-icons">{this.props.icon}</i>
                    <p>{this.props.name}</p>
                 </Link>
            </li>
        );
    }
}