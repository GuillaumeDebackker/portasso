import React from "react";

import {Editor} from '@tinymce/tinymce-react';
import Axios from 'axios';
import Select from 'react-select';
import {Redirect} from 'react-router-dom';

import Alert from "../box/Alert";
import FormGroup from "../box/FormGroup";
import ConfigWebsite from '../../config/ConfigWebsite';
import ActualityPreview from "../actuality/ActualityPreview";

export default class ActualityCreate extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            _id: (localStorage.getItem('user') === undefined || localStorage.getItem('user') === null ? null : localStorage.getItem('user')),
            user: null,
            rank: null,
            associations: [],
            preview: false,
            actuality: {
                editor: {},
                title: "",
                text: "",
                file: []
            },

            message_type: "",
            message: ""
        }

        this.hiddenFileInput = React.createRef();
    }

    async componentDidMount(){
        const user = await Axios.post(`${ConfigWebsite.HOST + ":" + ConfigWebsite.PORTAPI}/user/`, {id: this.state._id});
        const associations = await Axios.post(`${ConfigWebsite.HOST + ":" + ConfigWebsite.PORTAPI}/association/findall`, {"members.value" : this.state._id});
        this.setState({user:user.data.user,rank: user.data.rank,associations:associations.data});
    }

    checkPermission(){
        if(this.state.rank === null) return true;
        for (const permission in this.state.rank.permission) {
            if(this.state.rank.permission[permission] === "ACTUALITY_CREATE") return true;
        }

        for(const association in this.state.associations){
            if(this.state.associations[association].actuality) return true;
        }
        return false;
    }

    onPreview = () => {
        const formData = new FormData();
        for (const fileKey in this.state.actuality.file) {
            const file = this.state.actuality.file[fileKey];
            formData.append(file.name,file);
        }

        Axios.post(`${ConfigWebsite.HOST}:${ConfigWebsite.PORTAPI}/upload`,formData).then(() => this.setState({preview: true}));
    }

    createActuality = async () => {
        if(this.state.actuality.title === "" || this.state.actuality.text.length < 200 || (this.state.actuality.editor.value === undefined || this.state.actuality.editor.value === "")){
            return this.setState({message_type: "danger",message: "Vous devez remplir tous les champs ou bien le texte est trop court (200 caractères)"});
        }
        const authors = this.getAuthor();
        const body = {
            title: this.state.actuality.title,
            text: this.state.actuality.text,
            author_type: authors['author_type'],
            author_id: authors['author_id'],
            files: this.convertFileToArrayName()
        }

        const {data} = await Axios.post(`${ConfigWebsite.HOST}:${ConfigWebsite.PORTAPI}/actuality/create`,body);
    }

    getAuthor = () => {
        const author = [];
        if(this.state.actuality.editor.value === this.state._id) {
            author['author_type'] = "user";
            author['author_id'] = this.state._id;
        }else{
            author['author_type'] = "association";
            author['author_id'] = this.state.actuality.editor.value;
        }
        return author;
    }

    convertFileToArrayName = () => {
        let files = [];
        for (const actualityKey in this.state.actuality.file) {
            if(typeof(this.state.actuality.file[actualityKey]) !== "object") continue;
            files.push(this.state.actuality.file[actualityKey].name);
        }
        return files;
    }

    render() {
        // SI LE JOUEUR N'EST PAS CONNECTER OU QU'IL N'AS PAS LA PERMISSION
        if(this.state._id === null || !this.checkPermission()) return <Redirect to={"/"}/>
        let options = [];
        if(this.state.rank === null) return options;

        for (const permission in this.state.rank.permission) {
            if(this.state.rank.permission[permission] === "ACTUALITY_CREATE") options.push({'label': this.state.user.firstname + " " + this.state.user.lastname, 'value': this.state.user._id});
        }

        for(const association in this.state.associations){
            if(this.state.associations[association].actuality) options.push({'label': this.state.associations[association].name, 'value': this.state.associations[association]._id});
        }


        return (
            <div className="content">
                <div className="container">
                    {
                        this.state.preview ? <ActualityPreview actuality={this.state.actuality} setState={state => this.setState(state)}/> : null
                    }
                    {
                        this.state.message_type !== "" ? <Alert type={this.state.message_type} message={this.state.message} setState={state => this.setState(state)}/> : null
                    }
                    <div className="row">
                        <div className="col-md-4">
                            <FormGroup type={"text"} keyGroup={"title"} placeholder={"Titre"} onChange={(event) => this.setState({actuality: {editor: this.state.actuality.editor,title: event.target.value, text: this.state.actuality.text,file: this.state.actuality.file}})}/>
                        </div>
                        <div className="col-md-3">
                            <FormGroup keyGroup={"title"} placeholder={"Titre"} children={
                                <Select options={options} onChange={(event) => this.setState({actuality: {editor: event,title: this.state.actuality.title, text: this.state.actuality.text,file: this.state.actuality.file}})}/>
                            }/>
                        </div>
                        <div className="actuality-create-button">
                            <button className="btn btn-outline-secondary" onClick={() => this.hiddenFileInput.current.click()}>Rajouter une image</button>
                            <input
                                type="file"
                                accept="image/png, image/jpeg"
                                ref={this.hiddenFileInput}
                                style={{display: 'none'}}
                                onChange={(event) => this.setState({actuality: {editor: this.state.actuality.editor,title: this.state.actuality.title, text: this.state.actuality.text,file: event.target.files}})}
                                multiple
                            />
                            <button className="btn btn-outline-info" onClick={this.onPreview}>Previsualiser</button>
                            <button className="btn btn-outline-primary" onClick={this.createActuality}>Envoyer</button>
                        </div>
                    </div>
                    <Editor
                        init={{
                            menubar: 'edit format',
                            plugins: 'link',
                            toolbar: 'undo redo | formatselect | fontselect bold italic fontsizeselect forecolor backcolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link',
                            content_css: 'dark',
                            skin: 'oxide-dark',
                            height: 400
                        }}

                        onEditorChange={(content) => this.setState({actuality: {editor: this.state.actuality.editor,title: this.state.actuality.title, text: content,file: this.state.actuality.file}})}
                    />
                </div>
            </div>
        );
    }
}