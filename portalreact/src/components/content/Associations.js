import React from 'react';
import Axios from "axios";
import {Link} from "react-router-dom";
import ConfigWebsite from "../../config/ConfigWebsite";

export default class Associations extends React.Component{

    constructor(props) {
        super(props);
        this.state = {
            associations: [],
            user: []
        }
    }

    async componentDidMount() {
        const {data} = await Axios.post(`${ConfigWebsite.HOST}:${ConfigWebsite.PORTAPI}/association/findall`);
        const user = await Axios.post(`${ConfigWebsite.HOST}:${ConfigWebsite.PORTAPI}/user/findall`);
        this.setState({associations: data,user: user.data});
    }

    render(){
        return (
            <div className="content">
                <div className="container">
                    <h3 className="text-center">Les associations</h3>

                    <div style={{width: 75 + "%",marginLeft: 12.5 + "%", paddingTop: 2 + "em"}}>
                        {
                            this.state.associations.map((association) => {
                                const images = require.context('../../../../public', true);
                                const loadImage = images(`./${association.filename}`).default;
                                const author = this.state.user.find(user => user._id === association.author);
                                return (
                                    <div className="card" key={association._id}>
                                        <div className="card-body">
                                            <div className="row">
                                                <div className="col-md-2">
                                                    <img className="association-image" alt="product" src={loadImage}/>
                                                </div>
                                                <div className="col-md-6">
                                                    <h4 className="association-text black">{association.name}</h4>
                                                    <p className="grey">{association.description}</p>

                                                    <p className="black">
                                                        <i className="fa fa-user" aria-hidden="true"></i> {author.firstname} {author.lastname}<br/>
                                                        <i className="fa fa-users" aria-hidden="true"></i> {association.members.length} membres
                                                    </p>
                                                </div>
                                                <div className="col-4">
                                                    {association.actuality ? <span className="labelled blue" style={{float: "right"}}>Actualité</span> : null}
                                                    <Link to={"association/" + association._id} className="association-button black">Fiche</Link>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                )
                            })
                        }
                    </div>
                </div>
            </div>
        );
    }
}