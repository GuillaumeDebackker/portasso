import React from "react";
import Image404 from '../../public/img/404.png';

export default class fileNotFound extends React.Component{

    render() {
        return (
            <div className="content">
                <div className="container">
                    <img src={Image404} alt={"not found"} style={{width: 100 + "%",height: 100 + "%"}}/>
                </div>
            </div>
        );
    }
}