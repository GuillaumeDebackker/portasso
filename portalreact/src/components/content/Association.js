import React from "react";
import Axios from "axios";
import ConfigWebsite from '../../config/ConfigWebsite';
import NetworkConfig from '../../config/network';
import DateFormat from '../../utils/DateFormat';
import {Link} from "react-router-dom";
//import {Link} from 'react-router-dom';

export default class Association extends React.Component{

    constructor(props) {
        super(props);
        this.state = {
            association: null,
            user: []
        }
    }

    async componentDidMount() {
        const {data} = await Axios.post(`${ConfigWebsite.HOST}:${ConfigWebsite.PORTAPI}/association/find`,{id: this.props.match.params.id});
        const user = await Axios.post(`${ConfigWebsite.HOST}:${ConfigWebsite.PORTAPI}/user/findall`);
        this.setState({association: data,user: user.data});
    }


    render() {
        if(this.state.association === null) return <div></div>

        const date = new Date(this.state.association.createdAt);
        const format = DateFormat(date,"%d %m %Y");

        const images = require.context('../../../../public', true);
        const loadImage = images(`./${this.state.association.filename}`).default;
        const author = this.state.user.find(user => user._id === this.state.association.author);
        return (
            <div className="content">
                <div className="container-fluid" style={{width: 75 +"%"}}>
                    <div className="row" style={{paddingTop: 2+"em"}}>
                        <div className="col-3">
                            <div className="card round">
                                <div className="card-body">
                                    <p className="text-center">
                                        <img src={loadImage} className="circle" style={{height: 106 + "px",width: 106 + "px"}} alt="Avatar" />
                                    </p>
                                    <hr />
                                    <p>
                                        <i className="fa fa-user black"> Président: {author.firstname} {author.lastname}</i>
                                    </p>
                                    <p>
                                        <i className="fa fa-users black"> Membres: {this.state.association.members.map((member) => {return member.label})}</i>
                                    </p>
                                    <hr/>
                                    {
                                        this.state.association.network.map((network) => {
                                            const id = NetworkConfig.find((n) => n.id === network.id).icone;
                                            return <p key={network.id}>
                                                <i className={"fa fa-" + id + " black"}> {network.text}</i>
                                            </p>
                                        })
                                    }
                                </div>
                            </div>
                        </div>

                        <div className="col-9">
                            <div className="card round">
                                <div className="card-body">
                                    <span style={{float: "right"}}>{format}</span>
                                    <h4 className="black">{this.state.association.name}</h4>
                                    <hr />
                                    <p className="black">{this.state.association.description}</p>
                                    <div className="float-left" style={{paddingTop: 30 + "px"}}>
                                        {
                                            this.state.association.email !== ""
                                                ? <Link to={{pathname: "/contact", state: {association: {"label": this.state.association.name, "value": this.state.association.email}}}} className="association-button black">Contact</Link>
                                                : null
                                        }
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}