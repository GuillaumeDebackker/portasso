import React from "react";

import Select from "react-select";
import Axios from "axios";

import Alerts from "../box/Alert";
import Callout from "../box/Callout";
import FormGroup from "../box/FormGroup";
import ConfigWebsite from "../../config/ConfigWebsite";

export default class Contact extends React.Component{

    constructor(props) {
        super(props);

        this.state = {
            associations: [],
            association: (this.props.location.state !== undefined ? this.props.location.state.association : null),
            email: "",
            subject: "",
            text: "",

            EMAILQUEUE: false,

            message_type: "",
            message: ""
        }
    }

    async componentDidMount() {
        const {data} = await Axios.post(`${ConfigWebsite.HOST}:${ConfigWebsite.PORTAPI}/association/findall`,{"email" : {"$exists" : true, "$ne" : ""}});
        const response = await Axios.post(`${ConfigWebsite.HOST}:${ConfigWebsite.PORTAPI}/config`,{file: "config.json"});
        let options = [];

        data.map((association) => {
            return options.push({
                'label': association.name,
                'value': association.email
            });
        });
        this.setState({associations:options,EMAILQUEUE: response.data.EMAILQUEUE});
    }

    submit = async() => {
        if(this.state.association === null || this.state.email === "" || this.state.subject === "" || this.state.text === "") return this.setState({message_type: "danger", message: "Il manque des informations"});
        else if(!this.hasMatches(this.state.email, "@")) return this.setState({message_type: "danger", message: "Vous devez mettre un mail valide"});

        const body = {
            from: this.state.email,
            to: this.state.association.value,
            subject: this.state.subject,
            page: "contact.html",
            data: {
                text: this.state.text
            },
            waitingqueue: this.state.EMAILQUEUE
        }
        const {data} = await Axios.post(`${ConfigWebsite.HOST}:${ConfigWebsite.PORTAPI}/email/sendmail`,body);
        this.setState({
            association: null,
            email: "",
            subject: "",
            text: "",

            message_type: data['type'],
            message: data['message']
        });
    }

    handleAssociation = (event) => {this.setState({association: event})}
    handleEmail = (event) => {this.setState({email: event.target.value})}
    handleSubject = (event) => {this.setState({subject: event.target.value})}
    handleText = (event) => {this.setState({text: event.target.value})}

    hasMatches(value,reg){
        reg = new RegExp(reg,"g");
        const matches = value.match(reg);
        return matches && matches.length > 0;
    }

    render() {
        return (
            <div className="content">
                <div className="container">
                    <h3 className="text-center">Contact</h3>

                    {
                        this.state.EMAILQUEUE ? <Callout type={"warning"} message={"Afin de sécuriser les associations, nous allons faire une vérification avant l'envoie d'un message"}/> : null
                    }
                    {
                        this.state.message_type !== "" ? <Alerts type={this.state.message_type} message={this.state.message} setState={state => this.setState(state)}/> : null
                    }

                    <div className="card card-contact">
                        <div className="card-header">
                            <h3 className="card-title">Contact</h3>
                        </div>
                        <div className="card-body">
                            <FormGroup label={"Association"} keyGroup={"association"} style={{width: 50 + "%"}} children={<Select options={this.state.associations} value={this.state.association} onChange={this.handleAssociation}/>}/>
                            <FormGroup keyGroup={"email"} type={"text"} label={"Email"} value={this.state.email} style={{width: 50 + "%"}} onChange={this.handleEmail}/>
                            <FormGroup keyGroup={"subject"} type={"text"} label={"Titre"} value={this.state.subject} style={{width: 50 + "%"}} onChange={this.handleSubject}/>

                            <div className="form-group">
                                <label htmlFor={"Message"}>Message</label>
                                <textarea id="Message" className="form-control" rows="4" cols="50" value={this.state.text} onChange={this.handleText}/>
                            </div>
                        </div>
                        <div className="card-footer">
                            <div style={{paddingLeft: 93 +"%"}}>
                                <button type="button" onClick={this.submit} className="btn btn-primary">Envoyer</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}