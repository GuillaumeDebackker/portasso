import React from "react";
import ProfilItem from '../../config/ProfilItem';
import {Redirect} from 'react-router-dom';

export default class Profil extends React.Component{

    constructor(props) {
        super(props);
        this.state = {
            parameter: 0,
            redirect: localStorage.getItem('user') === undefined || localStorage.getItem('user') === null
        }
    }

    render() {
        if(this.state.redirect) return <Redirect to={"/"}/>
        return (
            <div className="content">
                <div className="container-fluid">
                    <h3 className="text-center">Profile</h3>
                    <div className="row">
                        <div className="col-md-1"></div>
                        <div className="col-md-1">
                            <div className="card">
                                <div className="card-header" style={{height: 50 + "px"}}>
                                    <h3 className="card-title black">Paramêtre</h3>
                                </div>
                                <ul className="list-group list-group-flush">
                                    {
                                        ProfilItem.map((item) => {
                                            return <button key={item.id} className={"list-group-item" +(this.state.parameter === item.id ? "" : "")+ " fa fa-" + item.icone} onClick={() => this.setState({parameter: item.id})}> {item.name}</button>;
                                        })
                                    }
                                </ul>
                            </div>
                        </div>

                        <div className="col-md-8">
                            {ProfilItem[this.state.parameter].item}
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}