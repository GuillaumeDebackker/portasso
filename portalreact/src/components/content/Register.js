import React from 'react';
import RegisterForm from '../register/RegisterForm';
import LoginForm from '../register/LoginForm';
import Alert from '../box/Alert';

export default class Register extends React.Component{

    constructor(props){
        super(props);
        console.log(this.props);
        this.state = {
            message_type: (this.props.location.state !== undefined ? this.props.location.state.message_type : ""),
            message: (this.props.location.state !== undefined ? this.props.location.state.message : "")
        };
    }

    render(){
        return(
            <div className="content">
                <div className="container">
                    <h3 className="text-center">Connexion/S'enregistrer</h3>

                    {
                        (this.state.message !== "" ? <Alert message={this.state.message} type={this.state.message_type} setState={state => this.setState(state)} /> : null)
                    }

                    <div className="row">
                        <div className="col-sm-9 col-md-7 col-lg-5 mx-auto">
                            <LoginForm setState={state => this.setState(state)} />
                        </div>
                        <div className="col-sm-9 col-md-7 col-lg-5 mx-auto">
                            <RegisterForm setState={state => this.setState(state)}/>
                        </div>
                    </div>
                </div>
            </div>    
        );
    }
}