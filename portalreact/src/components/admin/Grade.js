import React from 'react';
import Axios from 'axios';
import RankViewList from '../rank/RankViewList';
import RankAdd from '../rank/RankAdd';
import RankEdit from '../rank/RankEdit';
import Alert from '../box/Alert';
import ConfigWebsite from "../../config/ConfigWebsite";
export default class Grade extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            rank: [],
            rankedit: null,
            mode: 0,
            message: "",
            message_type: ""
        }
    }

    async componentDidMount(){
        const {data} = await Axios.post(`${ConfigWebsite.HOST}:${ConfigWebsite.PORTAPI}/rank/findall`);
        this.setState({rank:data});
    }

    componentDidUpdate(){
        if(this.rankEdit && this.state.rankedit !== null) this.rankEdit.setState({title:this.state.rankedit.name,description:this.state.rankedit.description,permission:this.state.rankedit.permission});
    }

    updateRank = async(state) => {
        if(state === undefined) return;
        const {data} = await Axios.post(`${ConfigWebsite.HOST}:${ConfigWebsite.PORTAPI}/rank/findall`);
        this.setState({rank:data});
        this.setState(state);
    }

    render(){
        return(
            <div className="container-fluid">
                <div className="main-header">
                    <h3 className="main-title">
                        Grade
                    </h3>
                </div>

                <div className="content-admin">
                    {
                        (this.state.message !== "" ? <Alert message={this.state.message} type={this.state.message_type} setState={state => this.setState(state)} /> : null)
                    }
                    <RankViewList rank={this.state.rank} setState={state => this.setState(state)} update={this.updateRank}/>
                    {
                        (this.state.mode === 1 ? <RankAdd setState={this.updateRank} /> : null)
                    }
                    {
                        (this.state.mode === 2 ? <RankEdit rank={this.state.rankedit} setState={this.updateRank} ref={ref => (this.rankEdit = ref)} /> : null)
                    }
                </div>
            </div>
        );
    }
}