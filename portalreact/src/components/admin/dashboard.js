import React from 'react';
import BoxStat from '../box/BoxStat';
import Axios from 'axios';
import ConfigWebsite from "../../config/ConfigWebsite";

export default class Dashboard extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            user: 0,
            association: 0,
            email: 0
        }
    }

    async componentDidMount(){
        const {data} = await Axios.post(`${ConfigWebsite.HOST}:${ConfigWebsite.PORTAPI}/user/findall`);
        const association = await Axios.post(`${ConfigWebsite.HOST}:${ConfigWebsite.PORTAPI}/association/findall`);
        const email = await Axios.post(`${ConfigWebsite.HOST}:${ConfigWebsite.PORTAPI}/email/findall`);
        this.setState({user: data.length,association: association.data.length, email: email.data.length})
    }

    render(){
        return(
            <div className="container-fluid">
                <div className="main-header">
                    <h3 className="main-title">
                        Dashboard
                    </h3>
                </div>

                <div className="content-admin">
                    <div className="row">
                        <div className="col-lg-3 col-6">
                            <BoxStat background="bg-success" to={""} value="0" name="Actualité" icon="description"/>
                        </div>
                        <div className="col-lg-3 col-6">
                            <BoxStat background="bg-info" to={"/admin/association"} value={this.state.association} name="Associations" icon="group"/>
                        </div>
                        <div className="col-lg-3 col-6">
                            <BoxStat background="bg-danger" to={"/admin/user"} value={this.state.user} name="Utilisateurs" icon="person"/>
                        </div>
                        <div className="col-lg-3 col-6">
                            <BoxStat background="bg-warning" to={"/admin/email"} value={this.state.email} name="Email" icon="email"/>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}