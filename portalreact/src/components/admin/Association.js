import React from 'react';
import Axios from 'axios';

import AssociationViewList from '../association/AssociationViewList';
import AssociationAdd from '../association/AssociationAdd';
import AssociationEdit from '../association/AssociationEdit';
import ConfigWebsite from "../../config/ConfigWebsite";

export default class Association extends React.Component{
    
    constructor(props){
        super(props);
        this.state = {
            associations: [],
            user: [],
            associationEdit: null,
            mode: 0
        }
    }

    async componentDidMount(){
        await this.update();
    }

    update = async(state) => {
        const {data} = await Axios.post(`${ConfigWebsite.HOST}:${ConfigWebsite.PORTAPI}/user/findall`);
        const response = await Axios.post(`${ConfigWebsite.HOST}:${ConfigWebsite.PORTAPI}/association/findall`);
        this.setState(state);
        this.setState({user:data,associations: response.data});
    }

    componentDidUpdate(){
        if(this.associationEdit && this.state.associationEdit !== null) this.associationEdit.update(this.state.associationEdit);
    }

    render(){
        return(
            <div className="container-fluid">
                <div className="main-header">
                    <h3 className="main-title">
                        Associations
                    </h3>
                </div>
                <div className="content-admin">
                    <AssociationViewList setState={this.update} associations={this.state.associations} user={this.state.user}/>
                    {
                        (this.state.mode === 1 ? <AssociationAdd user={this.state.user} setState={this.update}/> : null)
                    }
                    {
                        (this.state.mode === 2 ? <AssociationEdit association={this.state.associationEdit} user={this.state.user} setState={this.update} ref={ref => (this.associationEdit = ref)}/> : null)
                    }
                </div>
            </div>
        );
    }
}