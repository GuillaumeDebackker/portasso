import React from 'react';
import Axios from "axios";

import ConfigWebsite from '../../config/ConfigWebsite';
import EmailViewList from '../email/EmailViewList';
import EmailCheckView from "../email/EmailCheckView";
import Alert from "../box/Alert";

export default class Email extends React.Component{

    constructor(props) {
        super(props);
        this.state = {
            emailqueue: [],
            config: "",
            toggleQueue: false,
            mode: 0,
            emailview: null,

            message: "",
            message_type: ""
        }
    }

    componentDidMount() {
        this.update();
    }

    async componentDidUpdate(prevProps, prevState, snapshot) {
        this.state.config['EMAILQUEUE'] = this.state.toggleQueue;
        await Axios.post(`${ConfigWebsite.HOST}:${ConfigWebsite.PORTAPI}/updateconfig`,{file: "config.json",data:this.state.config});
    }

    update = async (state) => {
        const {data} = await Axios.post(`${ConfigWebsite.HOST}:${ConfigWebsite.PORTAPI}/email/findall`);
        const config = await Axios.post(`${ConfigWebsite.HOST}:${ConfigWebsite.PORTAPI}/config`,{file: "config.json"});
        this.setState(state);
        this.setState({emailqueue: data,config: config.data,toggleQueue: config.data.EMAILQUEUE});
    }

    render(){
        return(
            <div className="container-fluid">
                <div className="main-header">
                    <h3 className="main-title">
                        Email
                    </h3>
                </div>
                <div className="content-admin">
                    <div className="float-right">
                        <div className="checkbox-container">
                            <p>Email Queue</p>
                            <input type="checkbox" id="toggle2" checked={this.state.toggleQueue} onChange={(event) => this.setState({toggleQueue: event.target.checked})}/>
                            <label htmlFor="toggle2" className={(this.state.toggleQueue ? "checked-toggle" : null)}></label>
                        </div>
                    </div>
                    <div className="emailqueue">
                        {
                            (this.state.message !== "" ? <Alert message={this.state.message} type={this.state.message_type} setState={state => this.setState(state)} /> : null)
                        }
                        <EmailViewList emailqueue={this.state.emailqueue} setState={(state) => this.setState(state)}/>
                    </div>
                    {
                        this.state.mode === 1
                            ? <EmailCheckView email={this.state.emailview} setState={state => this.update(state)}/>
                            : null
                    }
                </div>
            </div>
        );
    }
}