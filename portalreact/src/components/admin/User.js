import React from 'react';
import Axios from 'axios';
import Callout from '../box/Callout';
import UserList from '../user/UserList';
import UserEdit from '../user/UserEdit';
import ConfigWebsite from "../../config/ConfigWebsite";

export default class User extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            user: [],
            rank: [],
            useredit: null,
            mode: 0
        }
    }

    async componentDidMount(){
        this.update();
    }

    componentDidUpdate(){
        if(this.userEdit && this.state.useredit !== null) this.userEdit.setState({firstname:this.state.useredit.firstname,lastname:this.state.useredit.lastname,email:this.state.useredit.email,rank:this.state.useredit.role});
    }

    update = async(state) => {
        const {data} = await Axios.post(`${ConfigWebsite.HOST}:${ConfigWebsite.PORTAPI}/user/findall`);
        const response = await Axios.post(`${ConfigWebsite.HOST}:${ConfigWebsite.PORTAPI}/rank/findall`);
        this.setState(state);
        this.setState({user:data,rank:response.data})
    }

    render(){
        return(
            <div className="container-fluid">
                <div className="main-header">
                    <h3 className="main-title">
                        Utilisateurs
                    </h3>
                </div>

                <div className="content-admin">
                    <Callout type="warning" message="Modifier un utilisateur n'est pas sans risque. L'utilisateur se connecte avec son adresse mail et d'un mot de passe. Le mieux est de le modifier avec son accord"/>
                    <UserList user={this.state.user} rank={this.state.rank} setState={this.update}/>
                    {
                        (this.state.mode === 1 ? <UserEdit user={this.state.useredit} rank={this.state.rank} setState={this.update} ref={ref => (this.userEdit = ref)} /> : null)
                    }
                </div>
            </div>
        );
    }
}