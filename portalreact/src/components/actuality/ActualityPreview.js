import React from "react";


export default class ActualityPreview extends React.Component{

    constructor(props) {
        super(props);
        this.state = {
            actuality: this.props.actuality
        }
    }

    renderImage = () => {
        const images = require.context('../../../../public', true);
        const files = this.convertFilesToArray();
        const width = (files.length > 4 ? 25 : 100 / files.length);
        return (
            <div>
                {
                    files.map((file) => {
                        return <img alt={"file"} src={images(`./${file.name}`).default} key={file.name} className="modal-actuality-image" style={{width: width + "%"}}/>
                    })
                }
            </div>
        )
    }

    convertFilesToArray = () => {
        let files = [];
        for (const actualityKey in this.state.actuality.file) {
            if(typeof(this.state.actuality.file[actualityKey]) !== "object") continue;
            files.push(this.state.actuality.file[actualityKey]);
        }
        return files;
    }

    render() {
        return (
            <div className="modal-actuality-preview">
                <div className="modal-actuality-content">
                    <button type="button" onClick={() => this.props.setState({preview: false})} className="close">&times;</button>
                    <h3>Editer: {this.state.actuality.editor.label}</h3>
                    <h3>Title: {this.state.actuality.title}</h3>
                    <hr/>
                    <p dangerouslySetInnerHTML={{__html: this.state.actuality.text}}></p>
                    {
                        this.state.actuality.file.length !== 0 ? <this.renderImage /> : null
                    }
                </div>
            </div>
        );
    }
}