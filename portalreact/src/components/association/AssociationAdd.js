import React from 'react';
import Select from 'react-select';
import Axios from 'axios';
import FormGroup from "../box/FormGroup";
import AssociationNetwork from "./AssociationNetwork";
import ConfigWebsite from "../../config/ConfigWebsite";

export default class AssociationAdd extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            options: [],
            disabled: true,
            name: "",
            president: "",
            description: "",
            email: "",
            membres: [],
            actuality: false,
            file: null,
            network: []
        };
    }

    componentDidMount(){
        let options = [];
        this.props.user.forEach(user => {
            options.push({
                'label': user.firstname + " " + user.lastname,
                'value': user._id
            });
        });
        this.setState({options:options});
        this.buttonDisabled();
    }

    handleChangeName = (event) => this.setState({name: event.target.value});
    handleChangePresident = (event) => this.setState({president: event.value});
    handleChangeMembers = (event) => this.setState({membres: event});
    handleChangeActuality = (event) => this.setState({actuality: event.target.value});
    handleChangeDescription = (event) => this.setState({description: event.target.value});
    handleChangeEmail = (event) => this.setState({email: event.target.value});
    handleChangeIcone = (event) => this.setState({file: event.target.files[0]});

    buttonDisabled = () => {
        const disabled = this.state.name === "" || this.state.president === "" || this.state.description === "" || this.state.membres.length === 0 || this.state.file === null
        this.setState({disabled: disabled});
        setTimeout(this.buttonDisabled,1000);
    }

    submit = async() => {
        const body = {
            name: this.state.name,
            description: this.state.description,
            author: this.state.president,
            members: this.state.membres,
            email: this.state.email,
            file: this.state.file.name,
            actuality: this.state.actuality,
            network: this.state.network
        }

        const formData = new FormData();
        formData.append('file',this.state.file);

        await Axios.post(`${ConfigWebsite.HOST}:${ConfigWebsite.PORTAPI}/upload`,formData);
        await Axios.post(`${ConfigWebsite.HOST}:${ConfigWebsite.PORTAPI}/association/create`,body);
        this.props.setState({mode: 0});
    }

    render(){
        return(
            <div className="card">
                <div className="card-header bg-card">
                    <h3 className="card-title">Rajouter une association</h3>
                    <div className="card-tools">
                        <button type="button" onClick={() => this.props.setState({mode:0})} className="btn btn-tool">
                            <i className="fa fa-times"></i>
                        </button>
                    </div>
                </div>
                <div className="card-body">
                    <div className="row">
                        <div className="col-2">
                            <FormGroup label={"Nom"} type={"text"} keyGroup={"name"} onChange={this.handleChangeName}/>
                        </div>

                        <div className="col-md-4">
                            <FormGroup label={"Président"} keyGroup={"name"} children={<Select options={this.state.options} onChange={this.handleChangePresident}/>}/>
                        </div>

                        <div className="col-md-4">
                            <FormGroup label={"Membres"} keyGroup={"members"} children={<Select options={this.state.options} isMulti onChange={this.handleChangeMembers} />}/>
                        </div>

                        <div className="col-md-2">
                            <div className="form-group" style={{float: 'left'}}>
                                <label>Actualité</label>
                                <input type="checkbox" className="form-control" onChange={this.handleChangeActuality} />
                            </div>
                        </div>

                        <div className="col-md-9">
                            <FormGroup label={"email"} type={"text"} keyGroup={"email"} onChange={this.handleChangeEmail}/>
                        </div>
                        <div className="col-md-3">
                            <FormGroup label={"Icone"} type={"file"} keyGroup={"image"} file={this.state.file === null ? "Choisir un fichier" : this.state.file.name} onChange={this.handleChangeIcone}/>
                        </div>
                        <div className="col-md-12">
                            <div className="form-group">
                                <label htmlFor={"description"}>Description</label>
                                <textarea id="description" className="form-control" rows="4" cols="50" onChange={this.handleChangeDescription}/>
                            </div>
                        </div>
                    </div>

                    <br/>
                    <AssociationNetwork setState={(state) => this.setState(state)}/>
                </div>
                <div className="card-footer">
                    <div style={{paddingLeft:95 +"%"}}>
                        <button type="button" onClick={this.submit} disabled={this.state.disabled} className="btn btn-primary">Rajouter</button>
                    </div>
                </div>
            </div>
        )
    }
}