import React from "react";
import NetWork from '../../config/network';

export default class AssociationNetwork extends React.Component{

    constructor(props) {
        super(props);
        this.state = {
            network: (this.props.value === undefined ? [] : this.props.value),
            networkSelect: NetWork[0].text,
            textNetwork: ""
        }
    }

    addNetwork = () => {
        if(this.state.textNetwork === "") return;
        let network = this.state.network;
        network.push({id:this.state.networkSelect,text:this.state.textNetwork});
        this.setState({network: network,networkSelect: NetWork[0].text, textNetwork: ""});
        this.props.setState({network: this.state.network});
        this.addText.value = "";
    }

    removeNetwork = (event) => {
        const id = event.target.id;
        let network = this.state.network;
        let x = 0;
        network.map((networkSelected) => {
            if (networkSelected.id === id) network.splice(x, 1);
            return x++;
        });
        this.setState({network: network});
        this.props.setState({network: this.state.network});
    }

    render() {
        return (
            <div className="networkassociation">
                <div className="row">
                    <div className="col-md-11">
                        <div className="input-group">
                            <div className="input-group-prepend">
                                <select className="input-group-text" onChange={(event) => this.setState({networkSelect: event.target.value})}>
                                    {NetWork.map((network) => { return <option key={network.id}>{network.text}</option> })}
                                </select>
                            </div>
                            <input type="text" className="form-control" ref={ref => (this.addText = ref)} placeholder={"Mettez votre " + this.state.networkSelect} aria-label="Username" aria-describedby="basic-addon1" onChange={(event => this.setState({textNetwork: event.target.value}))}/>
                        </div>
                    </div>
                    <div className="col-md-1">
                        <div className="form-group" style={{paddingTop: 0.95 + "em"}}>
                            <button type="button" className="btn btn-primary" onClick={this.addNetwork}>Rajouter</button>
                        </div>
                    </div>
                </div>
                {
                    this.state.network.map((networkAdding) => {
                        return (
                            <div className="row">
                                <div className="col-md-11">
                                    <div className="input-group">
                                        <div className="input-group-prepend">
                                      <span className="input-group-text">
                                          {networkAdding.id}
                                      </span>
                                        </div>
                                        <input type="text" className="form-control" aria-describedby="basic-addon1" defaultValue={networkAdding.text} disabled/>
                                    </div>
                                </div>
                                <div className="col-md-1">
                                    <div className="form-group" style={{paddingTop: 0.95 + "em"}}>
                                        <button type="button" id={networkAdding.id} className="btn btn-danger" onClick={this.removeNetwork}>Enlever</button>
                                    </div>
                                </div>
                            </div>
                        );
                    })
                }
            </div>
        );
    }
}