import React from 'react';
import Select from 'react-select';
import Axios from 'axios';
import FormGroup from "../box/FormGroup";
import AssociationNetwork from "./AssociationNetwork";
import ConfigWebsite from "../../config/ConfigWebsite";

export default class AssociationAdd extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            options: [],
            id: this.props.association._id,
            name: this.props.association.name,
            president: this.props.association.author,
            description: this.props.association.description,
            members: this.props.association.members,
            email: this.props.association.email,
            actuality: this.props.association.actuality,
            network: this.props.association.network,
            file: this.props.association.filename,
            fileupdate: false
        };
    }

    async componentDidMount(){
        let options = [];
        this.props.user.forEach(user => {
            options.push({
                'label': user.firstname + " " + user.lastname,
                'value': user._id
            });
        });

        this.setState({options:options});
    }

    update(associationEdit){
        this.setState({
            id: associationEdit._id,
            name: associationEdit.name,
            president: associationEdit.author,
            description: associationEdit.description,
            members: associationEdit.members,
            actuality: associationEdit.actuality,
            network: associationEdit.network,
            file: associationEdit.filename,
            fileupdate: false
        })
    }

    handleChangeName = (event) => this.setState({name: event.target.value});
    handleChangePresident = (event) => this.setState({president: event.value});
    handleChangeMembers = (event) => this.setState({membres: event});
    handleChangeActuality = (event) => this.setState({actuality: event.target.value});
    handleChangeDescription = (event) => this.setState({description: event.target.value});
    handleChangeEmail = (event) => this.setState({email: event.target.value});
    handleChangeIcone = (event) => this.setState({file: event.target.files[0],fileupdate: true});

    submit = async() => {
        const body = {
            name: this.state.name,
            description: this.state.description,
            author: this.state.president,
            members: this.state.members,
            email: this.state.email,
            filename: (this.state.fileupdate ? this.state.file.name : this.state.file),
            actuality: this.state.actuality,
            network: this.state.network
        }


        if(this.state.fileupdate) {
            const formData = new FormData();
            formData.append('file',this.state.file);

            await Axios.post(`${ConfigWebsite.HOST}:${ConfigWebsite.PORTAPI}/upload`,formData);
        }

        await Axios.post(`${ConfigWebsite.HOST}:${ConfigWebsite.PORTAPI}/association/edit`,{id: this.state.id,body:body});
        this.props.setState({mode: 0});
    }

    render(){
        const author = this.props.user.find((user) => user._id === this.state.president);
        return(
            <div className="card">
                <div className="card-header bg-card">
                    <h3 className="card-title">Editer une association</h3>
                    <div className="card-tools">
                        <button type="button" onClick={() => this.props.setState({mode:0})} className="btn btn-tool">
                            <i className="fa fa-times"></i>
                        </button>
                    </div>
                </div>
                <div className="card-body">
                    <div className="row">
                        <div className="col-2">
                            <FormGroup label={"Nom"} type={"text"} keyGroup={"name"} value={this.state.name} onChange={this.handleChangeName}/>
                        </div>

                        <div className="col-md-4">
                            <FormGroup label={"Président"} keyGroup={"president"} children={<Select options={this.state.options} defaultValue={{label: author.firstname + " " + author.lastname,value:author._id}} onChange={this.handleChangePresident}/>}/>
                        </div>

                        <div className="col-md-4">
                            <FormGroup label={"Membres"} keyGroup={"members"} children={<Select options={this.state.options} isMulti defaultValue={this.state.members} onChange={this.handleChangeMembers} />}/>
                        </div>

                        <div className="col-md-2">
                            <div className="form-group" style={{float: 'left'}}>
                                <label>Actualité</label>
                                <input type="checkbox" className="form-control" onChange={this.handleChangeActuality} checked={this.state.actuality}/>
                            </div>
                        </div>

                        <div className="col-md-9">
                            <FormGroup label={"Email"} type={"text"} keyGroup={"email"} value={this.state.email} onChange={this.handleChangeEmail}/>
                        </div>

                        <div className="col-md-3">
                            <FormGroup label={"Icone"} type={"file"} keyGroup={"image"} file={(this.state.fileupdate ? this.state.file.name : this.state.file)} onChange={this.handleChangeIcone}/>
                        </div>

                        <div className="col-md-12">
                            <div className="form-group">
                                <label htmlFor={"description"}>Description</label>
                                <textarea id="description" className="form-control" rows="4" cols="50" onChange={this.handleChangeDescription} value={this.state.description}/>
                            </div>
                        </div>
                    </div>

                    <AssociationNetwork setState={(state) => this.setState(state)} value={this.state.network}/>
                </div>
                <div className="card-footer">
                    <div style={{paddingLeft:95 +"%"}}>
                        <button type="button" onClick={this.submit} disabled={this.state.disabled} className="btn btn-primary">Editer</button>
                    </div>
                </div>
            </div>
        )
    }
}