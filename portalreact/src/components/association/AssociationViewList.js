import React from 'react';
import AssociationView from './AssociationView';

export default class AssociationViewList extends React.Component{

    render(){
        return(
            <div className="card">
                <div className="card-header">
                    <h3 className="card-title black">Associations</h3>
                </div>
                <div className="card-body">
                    <table className="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th style={{width: 10 + "px"}}>ID</th>
                                <th>Nom</th>
                                <th>Description</th>
                                <th>Président</th>
                                <th>Membre</th>
                                <th>Actualité</th>
                                <th style={{width: 200 + "px"}}></th>
                            </tr>
                        </thead>
                        <tbody>
                        {
                            this.props.associations.map((association) => <AssociationView key={association._id} user={this.props.user} association={association} setState={state => this.props.setState(state)}/>)
                        }
                        </tbody>
                    </table>
                </div>
                <div className="card-footer">
                    <div style={{paddingLeft:95 +"%"}}>
                        <button type="button" onClick={() => this.props.setState({mode: 1})} className="btn btn-primary">Rajouter</button>
                    </div>
                </div>
            </div>
        );
    }
}