import React from 'react';
import Axios from "axios";
import ConfigWebsite from "../../config/ConfigWebsite";

export default class AssociationView extends React.Component{

    delete = async () => {
        await Axios.post(`${ConfigWebsite.HOST}:${ConfigWebsite.PORTAPI}/association/delete`,{id: this.props.association._id});
        this.props.setState();
    }

    render(){
        const author = this.props.user.find(user => user._id === this.props.association.author);

        return(
            <tr>
                <td>{this.props.association._id}</td>
                <td>{this.props.association.name}</td>
                <td>{this.props.association.description}</td>
                <td>{author.firstname + " " + author.lastname}</td>
                <td>{this.props.association.members.map((member) => {return member.label+ " ,"} )}</td>
                <td>{(this.props.association.actuality ? "oui" : "non")}</td>
                <td className="text-right">
                    <button type="button" className="btn btn-info btn-sm" onClick={() => this.props.setState({mode: 2,associationEdit: this.props.association})}>
                        <i className="fa fa-pencil"></i> Editer
                    </button>
                    {
                        (this.props.association.delete ? <button type="button" className="btn btn-danger btn-sm" onClick={this.delete}><i className="fa fa-trash"></i> Supprimer</button> : null)
                    }
                </td>
            </tr>
        );
    }
}