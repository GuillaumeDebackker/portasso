import React from 'react';
import {Alert} from 'react-bootstrap';

export default class Alerts extends React.Component{

    render(){
        return <Alert variant={this.props.type} onClose={() => this.props.setState({message: "",message_type: ""})} dismissible>{this.props.message}</Alert>
    }
}