import React from 'react';

export default class Callout extends React.Component{

    render(){
        return(
            <div className={"callout callout-" +this.props.type} {...this.props}>
                <h5>Attention !</h5>
                <p>{this.props.message}</p>
            </div>
        );
    }
}