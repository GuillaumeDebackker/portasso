import React from 'react';
import {Link} from 'react-router-dom';

export default class BoxStat extends React.Component{

    render(){
        return (
            <div className={"box-stat " + this.props.background}>
                <div className="inner">
                    <h3>{this.props.value}</h3>
                    <p>{this.props.name}</p>
                </div>
                <div className="icon">
                    <i className="material-icons">{this.props.icon}</i>
                </div>
                <Link to={this.props.to} className="box-stat-footer">
                    Plus d'Informations
                    <i className="fa fa-arrow-circle-right"></i>
                </Link>
            </div>
        );
    }
}