import React from 'react';

export default class FormGroup extends React.Component{

    render() {
        if(this.props.children){
            return (
                <div className="form-group" style={this.props.style}>
                    <label htmlFor={this.props.keyGroup}>{this.props.label}</label>
                    {this.props.children}
                </div>
            )
        }
        return(
            <div className="form-group" style={this.props.style}>
                <label htmlFor={this.props.keyGroup}>{this.props.label}</label>
                <div className="input-group-admin">
                    {
                        (this.props.type === 'file' ?
                                <div className="custom-file">
                                    <input type="file" className="custom-file-input" id={this.props.keyGroup} onChange={this.props.onChange} required/>
                                    <label className="custom-file-label" htmlFor="image">{this.props.file}</label>
                                </div>
                            :   <input type={this.props.type} className="form-control" id={this.props.keyGroup} value={this.props.value} placeholder={this.props.placeholder} onChange={this.props.onChange}/>
                        )
                    }
                </div>
            </div>
        );
    }
}