import React from 'react';

import EmailView from "./EmailView";

export default class EmailViewList extends React.Component{

    render(){
        return(
            <div className="card">
                <div className="card-header">
                    <h3 className="card-title black">Liste d'email</h3>
                </div>
                <div className="card-body p-0">
                    <table className="table table-striped projects">
                        <thead>
                            <tr>
                                <th style={{width: 1+"%"}}>#</th>
                                <th style={{width: 20+"%"}}>De</th>
                                <th style={{width: 30+"%"}}>A</th>
                                <th style={{width: 20+"%"}}>Sujet</th>
                                <th style={{width: 20+"%"}}>Texte</th>
                                <th style={{width: 20+"%"}}></th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                this.props.emailqueue.map((emailqueue) => {
                                    return <EmailView key={emailqueue._id} emailqueue={emailqueue} setState={(state) => this.props.setState(state)}/>
                                })
                            }
                        </tbody>
                    </table>
                </div>
            </div>
        );
    }
}