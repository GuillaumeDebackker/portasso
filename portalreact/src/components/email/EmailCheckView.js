import React from "react";
import Axios from "axios";
import ConfigWebsite from "../../config/ConfigWebsite";

export default class EmailCheckView extends React.Component{

    constructor(props) {
        super(props);
        this.state = {
            email: this.props.email
        }
    }

    send = async () => {
        const body = {
            from: this.state.email.from,
            to: this.state.email.to,
            subject: this.state.email.subject,
            page: "contact.html",
            data: {
                text: this.state.email.text
            },
            waitingqueue: false
        }
        const {data} = await Axios.post(`${ConfigWebsite.HOST}:${ConfigWebsite.PORTAPI}/email/sendmail`,body);
        await Axios.post(`${ConfigWebsite.HOST}:${ConfigWebsite.PORTAPI}/email/delete`,{id: this.state.email._id});
        this.props.setState({mode: 0,message: data.message, message_type: data.type});
    }

    delete = async () => {
        const {data} = await Axios.post(`${ConfigWebsite.HOST}:${ConfigWebsite.PORTAPI}/email/delete`,{id: this.state.email._id});
        this.props.setState({mode: 0,message: data.message, message_type: data.type});
    }

    render() {
        return (
            <div className="modal-email">
                <div className="modal-content-email">
                    <button type="button" onClick={() => this.props.setState({mode: 0})} className="close">&times;</button>
                    <h3>De: {this.state.email.from}</h3>
                    <h3>A: {this.state.email.to}</h3>
                    <hr/>
                    <p>Objet: {this.state.email.subject}</p>
                    <p>{this.state.email.text}</p>

                    <div className="modal-email-button">
                        <button type="button" className="btn btn-primary" onClick={this.send}>Envoyer</button>
                        <button type="button" className="btn btn-danger" onClick={this.delete}>Supprimer</button>
                    </div>
                </div>
            </div>
        );
    }
}