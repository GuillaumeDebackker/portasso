import React from 'react';

export default class EmailView extends React.Component{

    render(){
        return(
            <tr>
                <td>{this.props.emailqueue._id}</td>
                <td>{this.props.emailqueue.from}</td>
                <td>{this.props.emailqueue.to}</td>
                <td>{this.props.emailqueue.subject}</td>
                <td>{this.props.emailqueue.text}</td>
                <td className="text-right">
                    <button type="button" className="btn btn-info btn-sm" onClick={() => this.props.setState({mode: 1,emailview: this.props.emailqueue})}>
                        <i className="fa fa-pencil"></i> Voir
                    </button>
                </td>
            </tr>
        );
    }
}