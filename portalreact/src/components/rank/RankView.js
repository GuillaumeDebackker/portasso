import React from 'react';
import Axios from 'axios';
import ConfigWebsite from "../../config/ConfigWebsite";

export default class RankView extends React.Component{

    delete = async() => {
        const {data} = await Axios.post(`${ConfigWebsite.HOST}:${ConfigWebsite.PORTAPI}/rank/delete`,{id: this.props.rank.id});
        this.props.update({message:data.message,message_type:data.type});
    }

    render(){
        return(
            <tr>
                <td>{this.props.rank.id}</td>
                <td>{this.props.rank.name}</td>
                <td>{this.props.rank.description}</td>
                <td>{
                    Object.keys(this.props.rank.permission).map((permission) => {
                        return this.props.rank.permission[permission] + " ";
                    })
                }</td>
                <td className="text-right">
                    <button type="button" className="btn btn-info btn-sm" onClick={() => this.props.update({mode: 2,rankedit:this.props.rank})}>
                        <i className="fa fa-pencil"></i> Editer
                    </button>
                    {
                        (this.props.rank.delete ? <button type="button" className="btn btn-danger btn-sm" onClick={this.delete}><i className="fa fa-trash"></i> Supprimer</button> : null)
                    }
                </td>
            </tr>
        );
    }
}