import React from 'react';
import Permission from './Permission';
import permissionConfig from '../../config/permission';
import Axios from 'axios';
import ConfigWebsite from "../../config/ConfigWebsite";

export default class RankEdit extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            title: this.props.rank.name,
            description: this.props.rank.description,
            permission: this.props.rank.permission
        }
    }

    submit = async() => {
        const body = {
            "id": this.props.rank.id,
            "title" : this.state.title,
            "description" : this.state.description,
            "permission" : this.state.permission
        }


        const {data} = await Axios.post(`${ConfigWebsite.HOST}:${ConfigWebsite.PORTAPI}/rank/edit`,body);
        this.props.setState({message:data.message,message_type:data.type,mode: 0});
    }

    toggleChange = (id) => {
        let permission = this.state.permission;
        if(this.handleCheck(id)){
            var index = permission.indexOf(id);
            permission.splice(index,1);
        }else{
            permission.push(id);
        }
        this.setState({permission:permission});
    }

    updateTitle = (event) => this.setState({title: event.target.value});
    updateDescription = (event) => this.setState({description: event.target.value});

    handleCheck(id){
        return this.state.permission.some(element => element === id);
    }

    render(){
        return(
            <div className="card">
                <div className="card-header bg-card">
                    <h3 className="card-title">Editer d'un grade</h3>
                    <div className="card-tools">
                        <button type="button" onClick={() => this.props.setState({mode:0})} className="btn btn-tool">
                            <i className="fa fa-times"></i>
                        </button>
                    </div>
                </div>

                <div className="card-body">
                    <div className="row">
                        <div className="col-3">
                            <div className="form-group">
                                <label htmlFor="title">Titre</label>
                                <input type="text" className="form-control" key="title" value={this.state.title} onChange={this.updateTitle} required/>
                            </div>
                        </div>
                        <div className="col-9">
                            <div className="form-group">
                                <label htmlFor="description">Description</label>
                                <input type="text" className="form-control" key="description" onChange={this.updateDescription} value={this.state.description} required />
                            </div>
                        </div>
                    </div>

                    <hr />
                    <h3 className="text-center black">Permission</h3>
                    <div className="form-group pt-5">
                        <div className="row">
                            {
                                permissionConfig.map((permission) => {
                                    const hasPermission = this.state.permission.some(element => element === permission.id);
                                    return <Permission permission={permission} toggleChange={this.toggleChange} key={permission.id} required={hasPermission}/>
                                })
                            }
                        </div>
                    </div>
                </div>

                <div className="card-footer">
                    <div style={{paddingLeft:95 +"%"}}>
                        <button type="button" onClick={this.submit} className="btn btn-primary">Editer</button>
                    </div>
                </div>
            </div>
        );
    }
}