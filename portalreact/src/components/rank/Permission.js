import React from 'react';

export default class Permission extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            isChecked : false
        }
    }

    toggleChange = () => {
        const {toggleChange,permission} = this.props;
        this.setState(({ isChecked }) => (
            {
              isChecked: !isChecked,
            }
        ));

        toggleChange(permission.id);
    }

    render(){
        return(
            <div className="col-6">
                <div className="check-primary d-inline ml-2">
                    <input type="checkbox" className="option-input checkbox" key={this.props.permission.id} onChange={this.toggleChange} checked={this.props.required}/>
                    <label htmlFor={this.props.permission.id}>{this.props.permission.name}</label>
                </div>
                <span className="text"> : {this.props.permission.description}</span>
            </div>
        );
    }
}