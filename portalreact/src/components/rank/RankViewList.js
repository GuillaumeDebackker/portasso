import React from 'react';
import RankView from './RankView';

export default class RankViewList extends React.Component{

    render(){
        return(
            <div className="card">
                <div className="card-header">
                    <h3 className="card-title black">Liste de grade</h3>
                </div>
                <div className="card-body p-0">
                    <table className="table table-striped projects">
                        <thead>
                            <tr>
                                <th style={{width: 1+"%"}}>#</th>
                                <th style={{width: 20+"%"}}>Nom</th>
                                <th style={{width: 30+"%"}}>Description</th>
                                <th style={{width: 20+"%"}}>Permission</th>
                                <th style={{width: 20+"%"}}></th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                this.props.rank.map((rank) => {
                                    return <RankView rank={rank} key={rank.id} update={(state) => this.props.update(state)} />
                                })
                            }
                        </tbody>
                    </table>
                </div>
                <div className="card-footer">
                    <div style={{paddingLeft:95 +"%"}}>
                        <button type="button" onClick={() => this.props.setState({mode: 1})} className="btn btn-primary">Rajouter</button>
                    </div>
                </div>
            </div>
        );
    }
}