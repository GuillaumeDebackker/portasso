module.exports = {
    PORT: process.env.PORT || 3000,
    HOST: process.env.HOST || "http://localhost",
    PORTAPI: 8080,
}