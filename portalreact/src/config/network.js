
module.exports = [
    {
        id: "Facebook",
        icone: "facebook",
        text: "Facebook"
    },
    {
        id: "Instagram",
        icone: "instagram",
        text: "Instagram"
    },
    {
        id: "Twitter",
        icone: "twitter",
        text: "Twitter"
    },
    {
        id: "Youtube",
        icone: "youtube",
        text: "Youtube"
    },
    {
        id: "Internet",
        icone: "google",
        text: "Web"
    }
]