module.exports = [
    {
        id: "ACCESS_ADMIN",
        name: "Accès administrateur",
        description: "Accès au panel d'administrateur"
    },
    {
        id: "ACCESS_SITE",
        name: "Accès au site",
        description: "Accès au site"
    },
    {
        id: "ACTUALITY_CREATE",
        name: "Creation d'actualité",
        description: "Pouvoir créer des actualités"
    }
]