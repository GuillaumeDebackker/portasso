module.exports = [
    {
        to: "/admin",
        icon: "dashboard",
        name: "Dashboard"
    },
    {
        to: "/admin/grade",
        icon: "school",
        name: "Grade"
    },
    {
        to: "/admin/user",
        icon: "person",
        name: "Utilisateur"
    },
    {
        to: "/admin/association",
        icon: "group",
        name: "Associations"
    },
    {
        to: "/admin/email",
        icon: "email",
        name: "Email"
    }
]