import React from 'react';
import '../public/css/admin.css';

import NavbarAdmin from '../components/navbar/NavbarAdmin';
import FooterAdmin from '../components/navbar/FooterAdmin';
import Dashboard from '../components/admin/dashboard';
import Grade from '../components/admin/Grade';
import User from '../components/admin/User';
import Association from '../components/admin/Association';
import Email from "../components/admin/Email";

import { Route,Redirect } from 'react-router-dom';
import Axios from "axios";
import ConfigWebsite from "../config/ConfigWebsite";

export default class Admin extends React.Component{

    constructor(props) {
        super(props);
        this.state = {
            rank: null,
            redirect: false
        }
    }

    async componentDidMount() {
        if(localStorage.getItem('user') === undefined || localStorage.getItem('user') === null) return;

        let redirect = true;

        const {data} = await Axios.post(`${ConfigWebsite.HOST}:${ConfigWebsite.PORTAPI}/user`,{id: localStorage.getItem("user")});
        for (const permission in data.rank.permission) {
            if(data.rank.permission[permission] === "ACCESS_ADMIN") redirect = false;
        }
        this.setState({rank: data.rank, redirect: redirect});
    }

    render(){
        if(localStorage.getItem('user') === undefined || localStorage.getItem('user') === null || this.state.redirect) return <Redirect to={"/"}/>
        if(this.state.rank === null) return <div></div>
        return (
            <div className="wrapper">
                <NavbarAdmin />
                <div className="main-panel">
                    <Route path="/admin" exact component={Dashboard} />
                    <Route path="/admin/grade" exact component={Grade} />
                    <Route path="/admin/user" exact component={User} />
                    <Route path="/admin/association" exact component={Association} />
                    <Route path="/admin/email" exact component={Email} />
                    <FooterAdmin />
                </div>
            </div> 
        );
    }
}