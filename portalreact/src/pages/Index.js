import React from 'react';
import {
    Route, Switch
} from 'react-router-dom';

import '../public/css/style.css';

import Navbar from '../components/navbar/Navbar';
import Footer from '../components/navbar/Footer';
import Home from '../components/content/Home';
import Register from '../components/content/Register';
import fileNotFound from "../components/content/404";

// USER
import Verify from "../components/user/Verify";
import Profil from "../components/content/Profil";

// ASSOCIATION
import Associations from "../components/content/Associations";
import Association from "../components/content/Association";

//ACTUALITY
import Actuality from "../components/content/Actuality";
import ActualityCreate from "../components/content/ActualityCreate";

// CONTACT
import Contact from "../components/content/Contact";

export default class Index extends React.Component{

    render(){
        return (
            <div>
                <Navbar />
                <Switch>
                    <Route path="/" exact component={Home} />
                    <Route path="/user/verify/:id" component={Verify}/>
                    <Route path="/user/profil" exact component={Profil} />
                    <Route path="/association" exact component={Associations} />
                    <Route path="/association/:id" component={Association} />
                    <Route path="/actuality" exact component={Actuality} />
                    <Route path="/actuality/create" exact component={ActualityCreate} />
                    <Route path="/contact" component={Contact} />
                    <Route path="/register" exact component={Register} />
                    <Route component={fileNotFound}/>
                </Switch>
                <Footer />
            </div> 
        );
    }
}