import Express from 'express';
import {get, register} from 'express-decorators';
import FileUpload from 'express-fileupload';
import Mongoose from 'mongoose';
import bodyParser from 'body-parser';
import cors from 'cors';
import Verifier from 'email-verifier';
import nodemailer from 'nodemailer';
import nunjucks from 'nunjucks';
import fs from 'fs';

import Config from './config/Config';
import IndexController from './controller/IndexController';
import UserController from './controller/UserController';
import RankController from './controller/RankController';
import AssociationController from './controller/AssociationController';
import EmailController from './controller/EmailController';
import * as http from "http";

const app = Express();

app.use(FileUpload({createParentPath: true}))
app.use(cors());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

Mongoose.connect('mongodb://localhost:27017/portasso', {useNewUrlParser: true, useUnifiedTopology: true,useCreateIndex: true});
const database = Mongoose.connection;
database.on('error', console.error.bind(console, 'connection error:'));
database.once('open', function() {
    console.log('Connection established to bdd');
});

const transporter = nodemailer.createTransport({
    pool: true,
    host: 'smtp.portasso.fr',
    secure: false,
    port: 587,
    auth : {
        user: Config.DELIVERY.EMAIL,
        pass: Config.DELIVERY.PASSWORD
    },
    tls: {
        rejectUnauthorized: false
    }
});

const verifier = new Verifier(Config.API_VERIFY);

nunjucks.configure('email', {
    autoescape: true,
    express: app
});

const type = process.env.HTTPS? 'https' : 'http';

const https = require(type);
let server = null;

if(type === 'https') {
    const options = {
        cert: fs.readFileSync('/etc/letsencrypt/live/portasso.fr/fullchain.pem'),
        key: fs.readFileSync('/etc/letsencrypt/live/portasso.fr/privkey.pem')
    }

    server = https.createServer(options,app);
}else{
    server = https.createServer(app);
}

register(app,new IndexController(database,transporter));
register(app,new UserController(database,transporter,verifier));
register(app,new RankController(database,transporter));
register(app,new AssociationController(database,transporter));
register(app,new EmailController(database,transporter,verifier));

server.listen(8080, () => console.log("server started"));