import Nunjucks from 'nunjucks';

export default class Controller {

    constructor(db,transporter) {
        this.db = db;
        this.transporter = transporter;
        this.vars = [];
        this.debug = true;
    }

    render(request,response, StatusCode, data){
        if(this.debug) console.log("Debug Mode >> ",request.url,'\n','Status Code : ',StatusCode,'\n',data);
        response.status(StatusCode).json(data ? data : this.vars);
    }

    renderMail(response,from,to,subject,title,data){
        const render = Nunjucks.render(title,data);
        this.transporter.sendMail({
            from: from, // sender address
            to: to, // list of receivers
            subject: subject, // Subject line
            html: render
        }).then(() => response.status(200).json({type: "success", message: "L'email a bien été envoyé au destinataire"}))
            .catch((onerror) => {
                console.error(onerror);
                response.status(200).json({type: "danger", message:"Un problème est survenue lors de l'envoie. Veuillez contacter l'administration"});
            });

    }
}