import Mongoose from 'mongoose';

const EmailQueue = Mongoose.Schema({
    from: String,
    to: String,
    subject: String,
    text: String
}, { timestamps: true });

module.exports = EmailQueue;