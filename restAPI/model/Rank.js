import Mongoose from 'mongoose';

const Rank = Mongoose.Schema({
    id:{
        type:Number,
    },
    name:{
        type: String,
        unique: true
    },
    description: {
        type: String
    },
    permission: [],
    delete: {
        type: Boolean,
        default: true
    }
});

module.exports = Rank;