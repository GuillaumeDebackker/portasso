import Mongoose, { Schema } from 'mongoose';

const Association = Mongoose.Schema({
    name:{
        type: String,
        unique: true
    },
    description: {
        type: String
    },
    filename: String,
    author: Schema.Types.ObjectId,
    members: [],
    email: String,
    actuality: Boolean,
    network: [],
    delete: {
        type: Boolean,
        default: true
    }
}, { timestamps: true });

module.exports = Association;