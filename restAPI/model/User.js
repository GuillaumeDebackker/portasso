import Mongoose from 'mongoose';

const User = Mongoose.Schema({
    firstname: String,
    lastname: String,
    email: {
        type: String,
        unique: true
    },
    password: String,
    role: {
        type: Number,
        default: 1
    },
    active: {
        type: Boolean,
        default: false
    }
},{ timestamps: { createdAt: 'createDate' } });

module.exports = User;