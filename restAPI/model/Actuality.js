import Mongoose from 'mongoose';

const Actuality = Mongoose.Schema({
    title: String,
    text: String,
    author: {
        author_type: String,
        author_id: String
    },
    files: []
}, {timestamp: true});