import { basePath as BasePath, get as Get, post as Post, use as Use} from 'express-decorators';
import Controller from "../core/Controller";
import ActualitySchem from '../model/Actuality';

@BasePath('/actuality')
export default class ActualityController extends Controller{

    constructor(db,transporter){
        super(db,transporter);
    }

    @Post('/findall')
    findallActuality(req,res){
        const Actuality = this.db.model('actuality', ActualitySchem);
        const query = Actuality.find(req.body);

        query.exec((err,data) => {
            if(err) console.log(err);

            this.render(req,res,200,data);
        });
    }

    @Post('/create')
    createActuality(req,res){
        const {title,text,author_type,author_id, files} = req.body;
        const Actuality = this.db.model('actuality', ActualitySchem);

        const ActualityRequest = new Actuality({
            title: title,
            text: text,
            author: {
                author_type:author_type,
                author_id: author_id,
            },
            files: files
        });

        ActualityRequest.save((err,actuality) => {
            if(err){
                this.render(req,res,200,{message: "Problème pour la création d'actualité",type:"danger"});
            }else{
                this.render(req,res,200,{message: "L'actualité a bien été crée",type:"success",actuality:actuality});
            }
        });
    }
}