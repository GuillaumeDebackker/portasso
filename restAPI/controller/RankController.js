import { basePath as BasePath, get as Get, post as Post, use as Use} from 'express-decorators';
import Controller from '../core/Controller';
import RankSchem from '../model/Rank';

@BasePath('/rank')
export default class RankController extends Controller{

    constructor(db,transporter){
        super(db,transporter);
    }

    @Post('/findall')
    findallRank(req,res){
        const Rank = this.db.model('rank', RankSchem);
        const query = Rank.find();

        query.exec((err,data) => {
            this.render(req,res,200,data);
        });
    }

    @Post('/create')
    createRank(req,res){
        const Rank = this.db.model('rank', RankSchem);
        const lastRank = Rank.find({}).sort({_id: -1}).limit(1);
        lastRank.exec((err,rank) => {
            if(err) console.log(err);
            let id = rank[0].id + 1
            let title = req.body['title'];
            let description = req.body['description'];
            let permission = req.body['permission'];

            const RankRequest = new Rank({
                id: id,
                name: title,
                description: description,
                permission: permission
            });
    
            RankRequest.save((errRank) => {
                if(errRank){
                    //console.log(err);
                    if(errRank.code === 11000 || errRank.code === 11001){
                        this.render(req,res,200,{message: "Le nom est déjà utilisé",type:"danger"});
                    }else{
                        this.render(req,res,200,{message: "Le nom est déjà utilisé",type:"danger"});
                    }
                }else{
                    this.render(req,res,200,{message: "Le grade a bien été crée",type:"success",rank:RankRequest});
                }
            });
        });
    }

    @Post('/edit')
    async editRank(req,res){
        const {body} = req;
        const Rank = this.db.model('rank', RankSchem);

        let id = body['id'];
        let title = body['title'];
        let description = body['description'];
        let permission = body['permission'];

        const RankUpdate = await Rank.updateOne({id:id},{
            name: title,
            description: description,
            permission: permission
        });

        this.render(req,res,200,{message: "Le grade a bien été crée",type:"success",rank:RankUpdate});
    }

    @Post('/delete')
    deleteRank(req,res){
        const Rank = this.db.model('rank', RankSchem);
        Rank.deleteOne({id:req.body.id}).then(() => this.render(req,res,200,{message: "Le grade a bien été supprimé",type:"success"}));
    }
}