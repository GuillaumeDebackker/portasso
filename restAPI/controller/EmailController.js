import { basePath as BasePath, get as Get, post as Post, use as Use} from 'express-decorators';

import Controller from '../core/Controller';
import EmailQueueSchema from '../model/EmailQueue';

@BasePath('/email')
export default class EmailController extends Controller{

    constructor(db,transporter,verifier){
        super(db,transporter);

        this.verifier = verifier;
    }

    @Post('/sendmail')
    sendmail(req,res){
        const {from,to,subject,page,data,waitingqueue} = req.body;
        const EmailQueue = this.db.model('emailqueue', EmailQueueSchema);

        this.verifier.verify(from, (err,dataVerifier) => {
            if(dataVerifier.smtpCheck === 'false'){
                this.render(req,res,200,{type:"danger",message: "Vous devez mettre une adresse email valide"});
            }else{
                if(waitingqueue){
                    const email = new EmailQueue({
                        from: from,
                        to: to,
                        subject: subject,
                        text: data.text
                    });

                    email.save((error) => {
                        if(error){
                            console.log(error);
                            this.render(req,res,200, {type:"danger", message: "Une erreur est survenue"});
                        }else{
                            this.render(req,res,200, {type:"success", message: "Votre mail a bien été mit dans la liste d'attente"});
                        }
                    });
                }else{
                    this.renderMail(res,from,to,subject,page,data);
                }
            }
        });
    }

    @Post('/findall')
    findallEmailQueue(req,res){
        const EmailQueue = this.db.model('emailqueue', EmailQueueSchema);
        const query = EmailQueue.find(req.body);
        query.exec((err,data) => {
            if(err) console.log(err);

            this.render(req,res,200,data);
        });
    }

    @Post('/delete')
    delete(req,res){
        const EmailQueue = this.db.model('emailqueue', EmailQueueSchema);
        EmailQueue.deleteOne({_id:req.body.id}).then(() => this.render(req,res,200,{message: "Le mail a bien été supprimé",type:"success"}));
    }
}