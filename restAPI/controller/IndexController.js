import { basePath as BasePath, get as Get, post as Post, use as Use} from 'express-decorators';
import Controller from '../core/Controller';
import fs from 'fs';

@BasePath('/')
export default class IndexController extends Controller{

    constructor(db,transporter){
        super(db,transporter);
        this.path = process.cwd().replace("restAPI","");
    }

    @Post('/config')
    config(req,res){
        const {file} = req.body;
        fs.readFile('./config/' + file , (error,data) => {
            if(error) return console.log(error);
            let jsonParsed = JSON.parse(data);
            this.render(req,res,200,jsonParsed);
        });
    }

    @Post('/updateconfig')
    updateconfig(req,res){
        const {file,data} = req.body;
        fs.writeFile('./config/' + file , JSON.stringify(data, null, 4), (err) => {
            if (err) {
                console.log(`Error writing file: ${err}`);
            }else{
                this.render(req,res,200, {});
            }
        });
    }

    @Post('/upload')
    upload(req,res){
        if(!req.files) return this.render(req,res,500,{message: "File is not found"});

        const files = req.files;
        for (const fileKey in files) {
            const file = files[fileKey];
            file.mv(`${this.path}/public/${file.name}`, function (error) {
                if (error) {
                    console.log(error)
                }
            });
        }
        return this.render(req,res,200,{files: files});
    }
}