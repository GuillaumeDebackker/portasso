import { basePath as BasePath, get as Get, post as Post, use as Use} from 'express-decorators';
import Controller from '../core/Controller';
import UserSchem from '../model/User';
import RankSchem from '../model/Rank';
import Crypto from 'crypto';
@BasePath('/user')
export default class UserController extends Controller{

    constructor(db,transporter,verifier){
        super(db,transporter);
        this.verifier = verifier;
    }

    @Post('/')
    indexUser(req,res){
        const {id} = req.body;
        const User = this.db.model('user', UserSchem);
        const Rank = this.db.model('rank', RankSchem);
        const queryUser = User.findOne({_id:id});

        queryUser.exec((err,user) => {
            if(user){
                const queryRank = Rank.findOne({id:user.role});
                queryRank.exec((err,rank) =>{
                    if(rank){
                        this.render(req,res,200,{user:user,rank:rank});
                    }else{
                        this.render(req,res,200,{user: user,rank:null});
                    }
                });
                
            }else{
                this.render(req,res,200,{user: null,rank:null});
            }
        });
    }

    @Post('/findall')
    findallUser(req,res){
        const User = this.db.model('user', UserSchem);
        const query = User.find(req.body);

        query.exec((err,data) => {
            this.render(req,res,200,data);
        });
    }

    @Post('/register')
    registerUser(req,res){
        const {firstname,lastname,email,password} = req.body;
        const User = this.db.model('user', UserSchem);

        this.verifier.verify(email,(err,data) => {
            if(data.smtpCheck === 'false'){
                this.render(req,res,200,{message: "Vous devez mettre une adresse email valide",type:"danger"});
            }else{
                const passwordHash = Crypto.createHash('sha256').update(password).digest('hex');

                const userRequest = new User({
                    firstname,
                    lastname,
                    email,
                    password: passwordHash
                });

                userRequest.save((err) => {
                    if(err){
                        if(err.code === 11000 || err.code === 11001){
                            this.render(req,res,200,{message: "L'email est déjà utilisé",type:"danger"});
                        }
                    }else{
                        this.renderMail('delivery@portasso.fr',email,'Creation du compte','register.html',{url:`http://localhost:3000/user/verify/${userRequest._id}`});
                        this.render(req,res,200,{message: "Le compte a bien été crée, un mail a été envoyé pour valider votre compte",type:"success",user:userRequest});
                    }

                });
            }
        });
    }

    @Post('/login')
    login(req,res){
        const {emailLogin,passwordLogin} = req.body;
        const User = this.db.model('user', UserSchem);
        const passwordHash = Crypto.createHash('sha256').update(passwordLogin).digest('hex');

        const query = User.findOne({
            email: emailLogin,
            password: passwordHash
        });

        query.exec((err,user) => {
            if(user){
                if(user.active) {
                    this.render(req,res,200,{userId: user._id});
                }else{
                    this.render(req,res,200,{message: "Votre compte n'est pas activé, un mail a été envoyé pour l'activer",type:"danger"});
                }
            }else{
                this.render(req,res,200,{message: "L'email ou le mot de passe est incorrect",type:"danger"});
            }
        });
    }

    @Post('/validate')
    async validate(req,res){
        const {id} = req.body;
        const User = this.db.model('user', UserSchem);

        const UserUpdate = await User.updateOne({_id:id},{
            active: true,
        }).then(() => this.render(req,res,200,{message: "Votre compte est bien activé",type:"success"}));
    }

    @Post('/edit')
    async editUser(req,res){
        const {body} = req;
        const User = this.db.model('user', UserSchem);

        const UserUpdate = await User.updateOne({_id:body['_id']},{
            firstname: body['firstname'],
            lastname: body['lastname'],
            email: body['email'],
            role: body['rank']
        });

        this.render(req,res,200,{message: "Le grade a bien été crée",type:"success",user:UserUpdate});
    }
}