import { basePath as BasePath, get as Get, post as Post, use as Use} from 'express-decorators';
import Controller from '../core/Controller';
import AssociationSchem from '../model/Association';

@BasePath('/association')
export default class AssociationController extends Controller{

    constructor(db,transporter){
        super(db,transporter);
    }

    @Post('/findall')
    findallAssociation(req,res){
        const Association = this.db.model('association', AssociationSchem);
        const query = Association.find(req.body);
        query.exec((err,data) => {
            if(err) console.log(err);

            this.render(req,res,200,data);
        });
    }

    @Post('/find')
    findAssociation(req,res){
        const Association = this.db.model('association', AssociationSchem);
        const query = Association.findOne({_id:req.body.id});

        query.exec((err,data) => {
            this.render(req,res,200,data);
        });
    }

    @Post('/create')
    createAssociation(req,res){
        const Association = this.db.model('association', AssociationSchem);
        const {name,description,author,members,email,actuality,file,network} = req.body;

        const AssociationRequest = new Association({
            name: name,
            description: description,
            author: author,
            members: members,
            email: email,
            filename: file,
            actuality: (actuality === "on"),
            network: network
        });

        AssociationRequest.save((err,association) => {
            if(err){
                this.render(req,res,200,{message: "Le nom est déjà utilisé",type:"danger"});
            }else{
                this.render(req,res,200,{message: "L'association a bien été crée",type:"success",association:association});
            }
        });
    }

    @Post('/edit')
    async editAssociation(req,res){
        const {id,body} = req.body;
        const Association = this.db.model('association', AssociationSchem);

        const AssociationUpdate = await Association.updateOne({_id:id},{
            name: body['name'],
            description: body['description'],
            author: body['author'],
            members: body['members'],
            email: body['email'],
            actuality: (body['actuality'] === "on"),
            filename: body['filename'],
            network: body['network']
        });

        this.render(req,res,200,{message: "Le grade a bien été éditer",type:"success",association:AssociationUpdate});
    }

    @Post('/delete')
    deleteAssociation(req,res){
        const Association = this.db.model('association', AssociationSchem);
        Association.deleteOne({_id:req.body.id}).then(() => this.render(req,res,200,{message: "L'association a bien été supprimé",type:"success"}));
    }
}